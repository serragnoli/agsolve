# README #

### What is this repository for? ###

* The parser converts lines of logs into spreadsheet-friendly format
* Version: 0.5.0

### Architecture ####

* Architecture overview
![agsolve_0_5_0.jpg](https://bitbucket.org/repo/5gXEqG/images/4042452938-agsolve_0_5_0.jpg)

### How do I get set up? ###

* The application is in the JAR (Java ARchive) named **console-0.5.0-SNAPSHOT-jar-with-dependencies.jar**
* Dependencies
	* JRE (Java Runtime Environment 7 or higher)
* Test coverage can be found [here](http://serragnoli.bitbucket.org/agsolve/parser/v0.5.0.html)
* Before running the application
	* Download Java Runtime from [here](http://www.oracle.com/technetwork/java/javase/downloads/java-se-jre-7-download-432155.html)
	* Extract/install the downloaded file where you can easily find it.
	* Create the environment variable 'JAVA_HOME' pointing to where you extracted/installed Java
	* Go to the directory where 'console-0.5.0-SNAPSHOT-jar-with-dependencies.jar' is located

### How to run the application ####
* You must always start the application with: **java -jar console-0.5.0-SNAPSHOT-jar-with-dependencies.jar**
* Then add the following parameters as shown on the table below

Parameter Name | Optional? | Example of values 
---------------| :-------: | -----------------
origem         |   No	   | Log.txt
destino        |   Yes	   | Parsed.txt
quebra		   |   No	   | 52

* Example of syntaxes

Notice 													  | Syntax
--------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------
Use double-quotes (") around file name when it has spaces | java -jar console-0.5.0-SNAPSHOT-jar-with-dependencies.jar origem="file in this directory with spaces.txt" destino=fileInThisDirectoryWithoutSpaces.txt quebra=52
Files in the same directory, the **./** is optional | java -jar console-0.5.0-SNAPSHOT-jar-with-dependencies.jar origem="./file in this directory with spaces.txt" destino=./fileInThisDirectoryWithoutSpaces.txt quebra=52
Files in the parent directory, use **../** not optional in this case | java -jar console-0.5.0-SNAPSHOT-jar-with-dependencies.jar origem="../file in the parent directory with spaces.txt" destino=../fileInThisDirectoryWithoutSpaces.txt quebra=52

### Who do I talk to? ###

* Fabio Serragnoli - serragnoli@gmail.com
