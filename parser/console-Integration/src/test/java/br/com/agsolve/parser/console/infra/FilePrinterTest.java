package br.com.agsolve.parser.console.infra;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import br.com.agsolve.parser.console.controller.FilePrinter;
import br.com.agsolve.parser.console.infra.repo.IoFilePrinter;

@SuppressWarnings("boxing")
public class FilePrinterTest {

	private static final String VALID_FILE_LOCATION = "./target/test-classes/Sample_Hexa.txt";
	private static final String INEXISTENT_FILE = "./target/test-classes/DontExist.txt";
	private static final String INVALID_LOCATION = "http://www.google.com"; 
	private static final String SAVE_INTO = "./target/test-classes/integration_test_file.txt";
	private static final String FILE_CONTENT = "This is the content of the file";
	
	private FilePrinter filePrinter = new IoFilePrinter();
	
	@Test public void 
	should_load_file_if_one_exists() {
		List<String> loaded = filePrinter.load(VALID_FILE_LOCATION);
		
		assertThat(loaded.size(), is(68));
	}
	
	@Test(expected = IllegalArgumentException.class) public void 
	should_throw_exception_when_file_location_is_incorrect() {
		filePrinter.load(INEXISTENT_FILE);
	}
	
	@Test public void 
	should_save_file_into_specified_location() {
		filePrinter.save("File Content", SAVE_INTO);
		
		List<String> savedFile = filePrinter.load(SAVE_INTO);
		
		assertThat(savedFile.size(), is(1));
	}
	
	@Test(expected = IllegalArgumentException.class) public void 
	should_throw_exception_when_location_is_unreachable() {
		filePrinter.save(FILE_CONTENT, INVALID_LOCATION);
	}
}
