package br.com.agsolve.parser.console.infra;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.console.controller.StatsPrinter;
import br.com.agsolve.parser.console.infra.repo.ConsoleStatsPrinter;

public class ConsoleStatsPrinterTest {

	private static final String STATS_TEXT = "Stats text";
	private PrintStream out;
	private StatsPrinter statsPrinter;
	
	@Before public void
	setup() {
		out = mock(PrintStream.class);
		System.setOut(out);
		statsPrinter = new ConsoleStatsPrinter();
	}
	
	@Test public void 
	should_output_stats() {
		statsPrinter.print(STATS_TEXT);
		
		verify(out).println(STATS_TEXT);
	}
	
	@After public void
	cleanUp() {
		System.setOut(null);
	}
}
