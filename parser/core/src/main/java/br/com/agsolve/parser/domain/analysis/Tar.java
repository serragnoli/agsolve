package br.com.agsolve.parser.domain.analysis;

import static br.com.agsolve.parser.domain.analysis.OrderingConstants.TAR_AVERAGE;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.TAR_MAX;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.TAR_MIN;

import java.util.List;

import com.google.common.base.Objects;
import com.google.common.base.Splitter;

public class Tar {

	private List<String> splitArgs;
	private Average average;
	private Min min;
	private Max max;

	public Tar(String hexValue) {
		splitArgs(hexValue);
		average = new Average(splitArgs.get(0));
		min = new Min(splitArgs.get(1));
		max = new Max(splitArgs.get(2));
	}

	public Average average() {
		return average;
	}

	public Min min() {
		return min;
	}

	public Max max() {
		return max;
	}
	
	private void splitArgs(String hexValue) {
		splitArgs = Splitter.fixedLength(4)
				.splitToList(hexValue);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("splitArgs", splitArgs)
						.add("average", average)
						.add("min", min)
						.add("max", max)
						.toString();
	}

	public static class Average implements Reportable {

		private String hex;
		private double decimal;

		public Average(String hex) {
			this.hex = hex;
			convertToDecimal(hex);
		}

		@Override
		public String header() {
			return "Tar avg.";
		}

		public String hex() {
			return hex;
		}

		public double decimal() {
			return decimal;
		}

		@Override
		public int position() {
			return TAR_AVERAGE;
		}

		@Override
		public String value() {
			return String.valueOf(decimal);
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(this)
							.add("hex", hex)
							.add("decimal", decimal)
							.toString();
		}
		
		private void convertToDecimal(String fromHex) {
			List<String> split = Splitter.fixedLength(2).splitToList(fromHex);			
			String reversedHex = split.get(1) + split.get(0);
			
			double converted = Integer.parseInt(reversedHex, 16);
			
			decimal = converted / 10;
		}
	}

	public static class Min implements Reportable {

		private String hex;
		private double decimal;

		public Min(String hex) {
			this.hex = hex;
			convertToDecimal(hex);
		}
		
		@Override
		public String header() {
			return "Tar min.";
		}

		public String hex() {
			return hex;
		}

		public double decimal() {
			return decimal;
		}

		@Override
		public int position() {
			return TAR_MIN;
		}

		@Override
		public String value() {
			return String.valueOf(decimal);
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(this)
							.add("hex", hex)
							.add("decimal", decimal)
							.toString();
		}
		
		private void convertToDecimal(String fromHex) {
			List<String> split = Splitter.fixedLength(2).splitToList(fromHex);
			String reversedHex = split.get(1) + split.get(0);
			
			double converted = Integer.parseInt(reversedHex, 16);
			
			decimal = converted / 10;
		}
	}

	public static class Max implements Reportable {

		private String hex;
		private double decimal;

		public Max(String hex) {
			this.hex = hex;
			convertToDecimal(hex);
		}

		@Override
		public String header() {
			return "Tar max.";
		}
		
		public String hex() {
			return hex;
		}

		public double decimal() {
			return decimal;
		}

		@Override
		public int position() {
			return TAR_MAX;
		}

		@Override
		public String value() {
			return String.valueOf(decimal);
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(this)
							.add("hex", hex)
							.add("decimal", decimal)
							.toString();
		}
		
		private void convertToDecimal(String fromHex) {
			List<String> split = Splitter.fixedLength(2).splitToList(fromHex);
			String reversedHex = split.get(1) + split.get(0);
			
			double converted = Integer.parseInt(reversedHex, 16);
			
			decimal = converted / 10;
		}
	}
}
