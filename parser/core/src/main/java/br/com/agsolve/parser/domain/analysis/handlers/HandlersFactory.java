package br.com.agsolve.parser.domain.analysis.handlers;

import org.apache.log4j.Logger;


public class HandlersFactory {

	private static final Logger LOG = Logger.getLogger(HandlersFactory.class);
	
	public Handler produceHandlers() {
		LOG.info("Entering: produceHandlers");
		
		Handler dateHandler = new DateHandler();
		Handler batteryHandler = new BatteryHandler();
		Handler internalTemperatureHandler = new InternalTemperatureHandler();
		Handler idHandler = new IdHandler();
		Handler tarHandler = new TarHandler();
		Handler uarHandler = new UarHandler();
		Handler radParHandler = new RadParHandler();
		Handler precipitationHandler = new PrecipitationHandler();
		
		dateHandler.add(batteryHandler);
		batteryHandler.add(internalTemperatureHandler);
		internalTemperatureHandler.add(idHandler);
		idHandler.add(tarHandler);
		tarHandler.add(uarHandler);
		uarHandler.add(radParHandler);
		radParHandler.add(precipitationHandler);
		
		LOG.info("Exiting: produceHandlers");
		LOG.debug("Returning: " + dateHandler);		
		return dateHandler;
	}
}
