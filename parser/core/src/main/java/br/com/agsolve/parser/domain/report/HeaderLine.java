package br.com.agsolve.parser.domain.report;

import java.util.ArrayList;
import java.util.List;

import br.com.agsolve.parser.domain.analysis.Reportable;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;

public class HeaderLine implements Printable {

	static HeaderLine EMPTY = new HeaderLine();
	
	private static final String CARRIAGE_RETURN = System.getProperty("line.separator");
	
	final private String tabSeparated;
	
	private HeaderLine(){
		tabSeparated = "No data to be displayed in this report";
	}
	
	public HeaderLine(List<Reportable> orderedReportables) {
		tabSeparated = parseAsTabSeparated(orderedReportables);
	}

	private String parseAsTabSeparated(List<Reportable> orderedReportables) {
		List<String> headers = new ArrayList<>();
		
		for (Reportable reportable : orderedReportables) {
			headers.add(reportable.header());
		}
		
		Joiner joiner = Joiner.on("\t");
		return joiner.join(headers);
	}

	@Override
	public String outputAsTabSeparated() {
		return tabSeparated + CARRIAGE_RETURN;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("tabSeparated", tabSeparated)
						.toString();
	}
}
