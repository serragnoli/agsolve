package br.com.agsolve.parser.domain.analysis;

import static br.com.agsolve.parser.domain.analysis.OrderingConstants.BATTERY;

import com.google.common.base.Objects;

public class Battery implements Reportable {

	private String hex;
	private double decimal;

	public Battery(String hexValue) {
		this.hex = hexValue;

		convertToDecimalFrom(hexValue);
	}
	
	public String hex() {
		return hex;
	}

	public double decimal() {
		return decimal;
	}

	private void convertToDecimalFrom(String hexValue) {
		double intermediateConvertion = Integer.parseInt(hexValue, 16);

		decimal = intermediateConvertion / 10;
	}

	@Override
	public String header() {
		return "Battery";
	}
	
	@Override
	public int position() {
		return BATTERY;
	}

	@Override
	public String value() {
		return String.valueOf(decimal);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("hex", hex)
				.add("decimal", decimal)
				.toString();
	}
}
