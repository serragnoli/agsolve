package br.com.agsolve.parser.domain.analysis.handlers;

import org.apache.log4j.Logger;

import br.com.agsolve.parser.domain.analysis.Analysis;

class InternalTemperatureHandler extends Handler {

	private static final Logger LOG = Logger.getLogger(InternalTemperatureHandler.class);

	@Override
	void performStep(Analysis analysis) {
		LOG.info("Entering: performStep");
		LOG.debug("Parameter: " + analysis) ;

		analysis.parseInternalTemperature();
		
		LOG.info("Exiting: performStep");
	}
}
