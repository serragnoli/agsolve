package br.com.agsolve.parser.domain.report;

import java.util.Comparator;

import br.com.agsolve.parser.domain.analysis.Reportable;

public class ReportableComparator implements Comparator<Reportable>{

	@Override
	public int compare(Reportable reportable1, Reportable reportable2) {
		return reportable1.position() - reportable2.position();
	}
}
