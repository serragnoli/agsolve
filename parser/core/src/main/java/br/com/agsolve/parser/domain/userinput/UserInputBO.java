package br.com.agsolve.parser.domain.userinput;

import java.util.Map;

import org.apache.log4j.Logger;

public class UserInputBO {

	private static final Logger LOGGER = Logger.getLogger(UserInputBO.class);
	
	public OriginalInput treat(Map<Object, Object> userParams) {
		LOGGER.info("Entering: treat()");
		LOGGER.debug("Parameters: " + userParams);

		OriginalInput originalInput = new OriginalInput(userParams);
		
		LOGGER.info("Exiting: treat()");
		LOGGER.debug("Return: " + originalInput);
		return originalInput;
	}
}
