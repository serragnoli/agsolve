package br.com.agsolve.parser.domain.analysis.handlers;

import org.apache.log4j.Logger;

import br.com.agsolve.parser.domain.analysis.Analysis;

class IdHandler extends Handler {

	private static final Logger LOG = Logger.getLogger(IdHandler.class);

	@Override
	void performStep(Analysis analysis) {
		LOG.info("Entering: performStep");
		LOG.debug("Parameter: " + analysis) ;

		analysis.parseId();

		LOG.info("Exiting: performStep");
	}
}
