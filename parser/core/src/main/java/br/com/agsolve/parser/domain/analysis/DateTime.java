package br.com.agsolve.parser.domain.analysis;

import static br.com.agsolve.parser.domain.analysis.OrderingConstants.DATE_TIME;
import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;

import java.util.List;

import com.google.common.base.Objects;
import com.google.common.base.Splitter;

public class DateTime implements Reportable {

	private static final String DATE_SEPARATOR = "-";
	private static final String DATE_TIME_SEPARATOR = " ";
	private static final String TIME_SEPARATOR = ":";

	private List<String> split;
	private String original;
	private Year year;
	private Month month;
	private Day day;
	private Hour hour;
	private Minute minute;
	private Second second;

	public DateTime(String originalValue) {
		this.original = originalValue;

		extractDate(originalValue);
		this.year = new Year(split.get(0));
		this.month = new Month(split.get(1));
		this.day = new Day(split.get(2));
		this.hour = new Hour(split.get(3));
		this.minute = new Minute(split.get(4));
		this.second = new Second(split.get(5));
	}

	@Override
	public int position() {
		return DATE_TIME;
	}

	@Override
	public String header() {
		return "Date/Time";
	}

	public String original() {
		return original;
	}

	public Year year() {
		return year;
	}

	public Month month() {
		return month;
	}

	public Day day() {
		return day;
	}

	public Hour hour() {
		return hour;
	}

	public Minute minute() {
		return minute;
	}

	public Second second() {
		return second;
	}

	@Override
	public String value() {
		StringBuilder builder = new StringBuilder(day.text()).append(DATE_SEPARATOR)
																.append(month.text())
																.append(DATE_SEPARATOR)
																.append(year.text())
																.append(DATE_TIME_SEPARATOR)
																.append(hour.text())
																.append(TIME_SEPARATOR)
																.append(minute.text())
																.append(TIME_SEPARATOR)
																.append(second.text());

		return builder.toString();
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("split", split)
						.add("original", original)
						.add("year", year)
						.add("month", month)
						.add("day", day)
						.add("hour", hour)
						.add("minute", minute)
						.add("second", second)
						.toString();
	}

	private void extractDate(String from) {
		split = Splitter.fixedLength(2)
						.splitToList(from);
	}

	static class Year {

		private static final int FOUR_DIGIT_YEAR = 2000;
		private int decimal;
		private String text;

		public Year(String text) {
			this.decimal = parseInt(text) + FOUR_DIGIT_YEAR;
			this.text = valueOf(decimal);
		}

		public String text() {
			return text;
		}

		public int decimal() {
			return decimal;
		}

		@Override
		public String toString() {
			return Objects.toStringHelper(this)
							.add("text", text)
							.add("decimal", decimal)
							.toString();
		}
	}

	static class Month {

		private String text;
		private int decimal;

		public Month(String text) {
			this.text = text;
			this.decimal = parseInt(text);
		}

		public String text() {
			return text;
		}

		public int decimal() {
			return decimal;
		}

		@Override
		public String toString() {
			return Objects.toStringHelper(this)
							.add("text", text)
							.add("decimal", decimal)
							.toString();
		}
	}

	static class Day {

		private String text;
		private int decimal;

		public Day(String text) {
			this.text = text;
			this.decimal = parseInt(text);
		}

		public String text() {
			return text;
		}

		public int decimal() {
			return decimal;
		}

		@Override
		public String toString() {
			return Objects.toStringHelper(this)
							.add("text", text)
							.add("decimal", decimal)
							.toString();
		}
	}

	static class Hour {

		private String text;
		private int decimal;

		public Hour(String text) {
			this.text = text;
			this.decimal = parseInt(text);
		}

		public String text() {
			return text;
		}

		public int decimal() {
			return decimal;
		}

		@Override
		public String toString() {
			return Objects.toStringHelper(this)
							.add("text", text)
							.add("decimal", decimal)
							.toString();
		}
	}

	static class Minute {

		private String text;
		private int decimal;

		public Minute(String text) {
			this.text = text;
			this.decimal = parseInt(text);
		}

		public String text() {
			return text;
		}

		public int decimal() {
			return decimal;
		}

		@Override
		public String toString() {
			return Objects.toStringHelper(this)
							.add("text", text)
							.add("decimal", decimal)
							.toString();
		}
	}

	static class Second {

		private String text;
		private int decimal;

		public Second(String text) {
			this.text = text;
			this.decimal = parseInt(text);
		}

		public String text() {
			return text;
		}

		public int decimal() {
			return decimal;
		}

		@Override
		public String toString() {
			return Objects.toStringHelper(this)
							.add("text", text)
							.add("decimal", decimal)
							.toString();
		}
	}
}
