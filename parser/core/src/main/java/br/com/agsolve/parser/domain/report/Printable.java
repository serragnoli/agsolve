package br.com.agsolve.parser.domain.report;

interface Printable {

	String outputAsTabSeparated();
}
