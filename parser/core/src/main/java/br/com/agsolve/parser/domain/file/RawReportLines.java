package br.com.agsolve.parser.domain.file;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.common.base.Objects;

import br.com.agsolve.parser.domain.userinput.OriginalInput;
import br.com.agsolve.parser.domain.userinput.RawInputLine;

public class RawReportLines {

	private static final Logger LOGGER = Logger.getLogger(RawReportLines.class);
	private List<RawReportingLine> reportingLines = new ArrayList<>();

	private int lineBreak;

	public RawReportLines(OriginalInput originalInput) {
		lineBreak = originalInput.lineBreak();
		keepOnlyReportingLines(originalInput);
	}

	public TransposedLines transposeReportingLines() {
		LOGGER.info("Entering: transposeReportingLines");

		TransposedLines transposedLines = new TransposedLines();

		for (RawReportingLine rawReportingLine : reportingLines) {
			List<TransposedLine> lines = rawReportingLine.transpose(lineBreak);
			transposedLines.addAll(lines);
		}

		LOGGER.info("Exiting: transposeReportingLines -returning " + transposedLines);
		return transposedLines;
	}

	public int size() {
		return reportingLines.size();
	}

	private void keepOnlyReportingLines(OriginalInput originalInput) {
		LOGGER.debug("Entering: keepOnlyReportingLines: -parameters " + originalInput);

		List<RawInputLine> linesRead = originalInput.linesRead();

		for (RawInputLine inputLine : linesRead) {
			if (inputLine.isGoodForReporting()) {
				RawReportingLine rawReportingLine = new RawReportingLine(inputLine);
				this.reportingLines.add(rawReportingLine);
			}
		}

		LOGGER.debug("Exiting keepOnlyReportingLines");
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("lineBreak", lineBreak)
						.add("reportingLines", reportingLines)
						.toString();
	}
}
