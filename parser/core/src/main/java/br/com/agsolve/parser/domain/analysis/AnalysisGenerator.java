package br.com.agsolve.parser.domain.analysis;

import org.apache.log4j.Logger;

import com.google.common.base.Objects;

import br.com.agsolve.parser.domain.analysis.handlers.Handler;
import br.com.agsolve.parser.domain.analysis.handlers.HandlersFactory;
import br.com.agsolve.parser.domain.file.TransposedLines;

public class AnalysisGenerator {

	private static final Logger LOGGER = Logger.getLogger(AnalysisGenerator.class);
	private HandlersFactory factory;

	public AnalysisGenerator(HandlersFactory factory) {
		this.factory = factory;
	}

	public Analysis generate(TransposedLines lines) {
		LOGGER.info("Entering: generate()");
		LOGGER.debug("Parameters: " + lines);

		Handler handler = factory.produceHandlers();

		Analysis analysis = new Analysis(lines);

		handler.handle(analysis);

		LOGGER.info("Exiting: generate()");
		LOGGER.debug("Returning: " + analysis);
		return analysis;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("factory", factory)
						.toString();
	}
}
