package br.com.agsolve.parser.domain.analysis;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.agsolve.parser.domain.file.TransposedLines;

import com.google.common.base.Objects;

public class AnalysisBO {

	private static final Logger LOGGER = Logger.getLogger(AnalysisBO.class);

	private AnalysisGenerator analysisGenerator;
	private Analysis analysis;

	@Autowired
	public AnalysisBO(AnalysisGenerator analysisGenerator) {
		this.analysisGenerator = analysisGenerator;
	}

	public Analysis generateAnalysis(TransposedLines transposedLines) {
		LOGGER.info("Entering: generateReport()");
		LOGGER.debug("Parameters: " + transposedLines);

		analysis = analysisGenerator.generate(transposedLines);

		LOGGER.info("Exiting: generateReport()");
		LOGGER.debug("Returning: " + analysis);
		return analysis;
	}

	public Analysis analysis() {
		return analysis;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("reportGenerator", analysisGenerator)
						.toString();
	}
	
	//Unit-test
	void setAnalysis(Analysis analysis) {
		this.analysis = analysis;
	}
}
