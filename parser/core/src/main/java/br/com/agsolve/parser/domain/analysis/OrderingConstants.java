package br.com.agsolve.parser.domain.analysis;

public final class OrderingConstants {

	//TODO fabio 6 Aug 2014 19:42:58: Convert to an Enum
	public static final int DATE_TIME = 1;
	public static final int BATTERY = 2;
	public static final int INTERNAL_TEMPERATURE = 3;
	public static final int ID = 4;
	public static final int TAR_AVERAGE = 5;
	public static final int TAR_MIN = 6;
	public static final int TAR_MAX = 7;
	public static final int UAR_AVERAGE = 8;
	public static final int UAR_MIN = 9;
	public static final int UAR_MAX = 10;
	public static final int RAD_PAR = 11;
	public static final int PRECIPITATION = 12;
	
	private OrderingConstants() {}
}
