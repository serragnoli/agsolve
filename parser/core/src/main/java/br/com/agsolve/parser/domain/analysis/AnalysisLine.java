package br.com.agsolve.parser.domain.analysis;

import java.util.ArrayList;
import java.util.List;

import br.com.agsolve.parser.domain.file.TransposedLine;

import com.google.common.base.Objects;

public class AnalysisLine {

	private String original;

	private Consumable consumable;
	private DateTime dateTime;
	private Battery battery;
	private InternalTemperature internalTemperature;
	private Id id;
	private Tar tar;
	private Uar uar;
	private RadPar radPar;
	private Precipitation precipitation;

	public AnalysisLine(TransposedLine line) {
		this.original = line.display();
		this.consumable = new Consumable(line.display());
	}

	public String original() {
		return original;
	}

	public Consumable consumable() {
		return consumable;
	}

	public void parseDate() {
		String dateText = consumable.consumeDate();

		dateTime = new DateTime(dateText);
	}

	public void parseBattery() {
		String batteryText = consumable.consumeBattery();

		battery = new Battery(batteryText);
	}

	public void parseInternalTemperature() {
		String internalTemperatureText = consumable.consumeInternalTemperature();

		internalTemperature = new InternalTemperature(internalTemperatureText);
	}

	public void parseId() {
		String idText = consumable.consumeId();

		id = new Id(idText);
	}

	public void parseTar() {
		String tarText = consumable.consumeTar();

		tar = new Tar(tarText);
	}

	public void parseUar() {
		String uarText = consumable.consumeUar();

		uar = new Uar(uarText);
	}

	public void parseRadPar() {
		String radParText = consumable.consumeRadPar();

		radPar = new RadPar(radParText);
	}

	public void parsePrecipitation() {
		String precipitationText = consumable.consumePrecipitation();

		precipitation = new Precipitation(precipitationText);
	}

	public DateTime dateTime() {
		return dateTime;
	}

	public Battery battery() {
		return battery;
	}

	public InternalTemperature internalTemperature() {
		return internalTemperature;
	}

	public Id id() {
		return id;
	}

	public Tar tar() {
		return tar;
	}

	public Uar uar() {
		return uar;
	}

	public RadPar radPar() {
		return radPar;
	}

	public Precipitation precipitation() {
		return precipitation;
	}

	public List<Reportable> reportables() {
		List<Reportable> reportables = new ArrayList<>();
		reportables.add(id);
		reportables.add(tar.min());
		reportables.add(dateTime);
		reportables.add(tar.max());
		reportables.add(battery);
		reportables.add(internalTemperature);
		reportables.add(tar.average());
		reportables.add(uar.min());
		reportables.add(radPar);
		reportables.add(uar.average());
		reportables.add(precipitation);
		reportables.add(uar.max());
		
		return reportables;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("original", original)
						.add("dateTime", dateTime)
						.add("battery", battery)
						.add("internalTemperature", internalTemperature)
						.add("id", id)
						.add("tar", tar)
						.add("uar", uar)
						.add("radPar", radPar)
						.add("precipitation", precipitation)
						.toString();
	}
}
