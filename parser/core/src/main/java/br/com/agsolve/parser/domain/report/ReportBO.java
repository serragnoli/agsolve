package br.com.agsolve.parser.domain.report;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.AnalysisLine;
import br.com.agsolve.parser.domain.analysis.Reportable;

public class ReportBO {
	
	private static final Logger LOG = Logger.getLogger(ReportBO.class);

	private ReportableSorter sorter;

	@Autowired
	public ReportBO(ReportableSorter sorter) {
		this.sorter = sorter;
	}

	public Report prepare(Analysis analysis) {
		LOG.info("Entering: prepare");
		LOG.debug("Parameters: " + analysis);
		
		if(analysis.isEmpty()) {
			return Report.EMPTY;
		}
		
		List<List<Reportable>> collatedReportables = new ArrayList<>();
		
		for(AnalysisLine line : analysis.lines()) {
			List<Reportable> sorted = sorter.sort(line.reportables());
			collatedReportables.add(sorted);
		}
		
		Report report = new Report(collatedReportables);
		
		LOG.info("Exiting: prepare");
		LOG.debug("Returning: " + report);
		return report;
	}
}
