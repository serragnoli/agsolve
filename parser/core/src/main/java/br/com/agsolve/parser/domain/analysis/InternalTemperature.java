package br.com.agsolve.parser.domain.analysis;

import static br.com.agsolve.parser.domain.analysis.OrderingConstants.INTERNAL_TEMPERATURE;

import com.google.common.base.Objects;

public class InternalTemperature implements Reportable {

	private String hex;
	private double celsius;

	public InternalTemperature(String hexValue) {
		this.hex = hexValue;
		
		convertToCelsiusFrom(hexValue);
	}
	
	public double inCelsius() {
		return celsius;
	}

	private void convertToCelsiusFrom(String hexValue) {
		double inDecimal = Integer.parseInt(hexValue, 16);
		
		double intermediate = inDecimal * 5 - 500;
		
		celsius = intermediate / 10;
	}

	public String hex() {
		return hex;
	}

	@Override
	public String header() {
		return "Tint";
	}
	
	@Override
	public int position() {
		return INTERNAL_TEMPERATURE;
	}

	@Override
	public String value() {
		return String.valueOf(celsius);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("hex", hex)
				.add("celsius", celsius)
				.toString();
	}
}
