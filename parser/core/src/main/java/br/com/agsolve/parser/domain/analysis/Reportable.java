package br.com.agsolve.parser.domain.analysis;

public interface Reportable {
	String header();
	String value();
	int position();
}
