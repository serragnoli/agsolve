package br.com.agsolve.parser.domain.analysis.handlers;

import org.apache.log4j.Logger;

import br.com.agsolve.parser.domain.analysis.Analysis;

class TarHandler extends Handler {

	private static final Logger LOG = Logger.getLogger(TarHandler.class);

	@Override
	void performStep(Analysis analysis) {
		LOG.info("Entering: performStep");
		LOG.debug("Parameter: " + analysis) ;

		analysis.parseTar();
		
		LOG.info("Exiting: performStep");
	}
}
