package br.com.agsolve.parser.actions;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.AnalysisBO;
import br.com.agsolve.parser.domain.file.FileBO;
import br.com.agsolve.parser.domain.file.TransposedLines;
import br.com.agsolve.parser.domain.report.Report;
import br.com.agsolve.parser.domain.report.ReportBO;
import br.com.agsolve.parser.domain.userinput.OriginalInput;
import br.com.agsolve.parser.domain.userinput.UserInputBO;

public class CreateReportFromRawLines {
	
	private static final Logger LOG = Logger.getLogger(CreateReportFromRawLines.class);
	private FileBO fileBO;
	private AnalysisBO analysisBO;
	private UserInputBO userInputBO;
	private ReportBO reportBO;

	@Autowired
	public CreateReportFromRawLines(UserInputBO userInputBO, FileBO fileBO, AnalysisBO analysisBO, ReportBO reportBO) {
		this.userInputBO = userInputBO;
		this.fileBO = fileBO;
		this.analysisBO = analysisBO;
		this.reportBO = reportBO;
	}
	
	public Report parseRawLines(Map<Object, Object> parameters) {
		LOG.info("Entering: parseRawLines");
		LOG.debug("Parameters: " + parameters);
		
		OriginalInput originalInput = userInputBO.treat(parameters);
		
		TransposedLines transposedLines = fileBO.filterReportData(originalInput);
		
		Analysis analysis = analysisBO.generateAnalysis(transposedLines);
		
		Report report = reportBO.prepare(analysis);
		
		LOG.info("Exiting: parseRawLines");
		LOG.debug("Return: " + report);
		return report;
	}
}
