package br.com.agsolve.parser.domain.file;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Objects;

public class TransposedLines {

	private List<TransposedLine> lines = new ArrayList<>();

	public int size() {
		return lines.size();
	}

	public void addAll(List<TransposedLine> transposedLines) {
		this.lines.addAll(transposedLines);
	}

	public List<TransposedLine> lines() {
		return lines;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("lines", lines)
						.toString();
	}
}
