package br.com.agsolve.parser.domain.analysis;

import static br.com.agsolve.parser.domain.analysis.OrderingConstants.ID;

import java.util.List;

import com.google.common.base.Objects;
import com.google.common.base.Splitter;

public class Id implements Reportable {

	private String hex;
	private int decimal;

	public Id(String hexValue) {
		this.hex = hexValue;

		this.decimal = convertToDecimal(hexValue);
	}

	@Override
	public String header() {
		return "Id";
	}

	public int convertToDecimal(String hexValue) {
		List<String> split = Splitter.fixedLength(2)
										.splitToList(hexValue);
		String reversedHex = split.get(1) + split.get(0);

		return Integer.parseInt(reversedHex, 16);
	}

	public int decimal() {
		return decimal;
	}

	public String hex() {
		return hex;
	}

	@Override
	public int position() {
		return ID;
	}

	@Override
	public String value() {
		return String.valueOf(decimal);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("hex", hex)
				.add("id", decimal)
				.toString();
	}
}
