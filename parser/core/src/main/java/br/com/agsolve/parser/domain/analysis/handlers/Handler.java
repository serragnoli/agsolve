package br.com.agsolve.parser.domain.analysis.handlers;

import org.apache.log4j.Logger;

import com.google.common.base.Objects;

import br.com.agsolve.parser.domain.analysis.Analysis;

public abstract class Handler {

	private static final Logger LOG = Logger.getLogger(Handler.class);
	
	private Handler next;

	public void add(Handler nextHandler) {
		this.next = nextHandler;
	}

	Handler next() {
		return next;
	}

	public void handle(Analysis analysis) {
		LOG.info("Entering: handle");
		LOG.debug("Parameters: " + analysis);
		
		performStep(analysis);
		invokeNext(analysis);
		
		LOG.info("Exiting: handler");
	}

	public void invokeNext(Analysis analysis) {
		if (null != next) {
			next.handle(analysis);
		}
	}

	abstract void performStep(Analysis analysis);

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("next", next)
						.toString();
	}
}
