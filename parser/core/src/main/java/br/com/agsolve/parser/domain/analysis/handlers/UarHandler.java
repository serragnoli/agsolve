package br.com.agsolve.parser.domain.analysis.handlers;

import org.apache.log4j.Logger;

import br.com.agsolve.parser.domain.analysis.Analysis;

class UarHandler extends Handler {

	private static final Logger LOG = Logger.getLogger(UarHandler.class);

	@Override
	void performStep(Analysis analysis) {
		LOG.info("Entering: performStep");
		LOG.debug("Parameter: " + analysis) ;

		analysis.parseUar();
		
		LOG.info("Exiting: performStep");
	}
}
