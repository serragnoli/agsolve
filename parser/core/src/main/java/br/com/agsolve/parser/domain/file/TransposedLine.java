package br.com.agsolve.parser.domain.file;

import com.google.common.base.Objects;

public class TransposedLine {

	private String line;

	public TransposedLine(String line) {
		this.line = line;
	}

	public String display() {
		return line;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("line", line)
						.toString();
	}
}
