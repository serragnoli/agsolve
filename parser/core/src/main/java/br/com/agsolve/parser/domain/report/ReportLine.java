package br.com.agsolve.parser.domain.report;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;

import br.com.agsolve.parser.domain.analysis.Reportable;

public class ReportLine implements Printable {

	private static final String CARRIAGE_RETURN = System.getProperty("line.separator");
	
	private final String tabSeparated;

	public ReportLine(List<Reportable> sorted) {
		tabSeparated = parseAsTabSeparated(sorted);
	}

	@Override
	public String outputAsTabSeparated() {
		return tabSeparated + CARRIAGE_RETURN;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("tabSeparated", tabSeparated)
						.toString();
	}
	
	private String parseAsTabSeparated(List<Reportable> sorted) {
		List<String> values = new ArrayList<>();
		
		for (Reportable reportable : sorted) {
			values.add(reportable.value());
		}
		
		Joiner joiner = Joiner.on("\t");
		return joiner.join(values);
	}
}
