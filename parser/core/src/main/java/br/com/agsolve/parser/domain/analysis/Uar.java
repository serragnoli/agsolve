package br.com.agsolve.parser.domain.analysis;

import static br.com.agsolve.parser.domain.analysis.OrderingConstants.UAR_AVERAGE;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.UAR_MAX;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.UAR_MIN;

import java.util.List;

import com.google.common.base.Objects;
import com.google.common.base.Splitter;

public class Uar {

	private List<String> splitArgs;
	private Average average;
	private Min min;
	private Max max;

	public Uar(String hexValues) {
		splitArgs(hexValues);
		average = new Average(splitArgs.get(0));
		min = new Min(splitArgs.get(1));
		max = new Max(splitArgs.get(2));
	}

	private void splitArgs(String hexValues) {
		splitArgs = Splitter.fixedLength(4)
							.splitToList(hexValues);
	}

	public Average average() {
		return average;
	}

	public Min min() {
		return min;
	}

	public Max max() {
		return max;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("splitArgs", splitArgs)
						.add("averaga", average)
						.add("min", min)
						.add("max", max)
						.toString();
	}

	public static class Average implements Reportable {

		private String hex;
		private double decimal;

		public Average(String hexValue) {
			hex = hexValue;
			convertToDecimalAndCalculate(hexValue);
		}
		
		@Override
		public String header() {
			return "Uar avg.";
		}

		public String hex() {
			return hex;
		}

		public double decimal() {
			return decimal;
		}

		@Override
		public int position() {
			return UAR_AVERAGE;
		}

		@Override
		public String value() {
			return String.valueOf(decimal);
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(this)
							.add("hex", hex)
							.add("decimal", decimal)
							.toString();
		}
		
		private void convertToDecimalAndCalculate(String hexValue) {
			List<String> split = Splitter.fixedLength(2).splitToList(hexValue);
			String invertedHex = split.get(1) + split.get(0);
			
			double converted = Integer.parseInt(invertedHex, 16);
			
			decimal = converted / 10;
		}
	}

	public static class Min implements Reportable  {

		private String hex;
		private double decimal;

		public Min(String hexValue) {
			hex = hexValue;
			convertToDecimalAndCalculate(hexValue);
		}
		
		@Override
		public String header() {
			return "Uar min.";
		}

		public String hex() {
			return hex;
		}

		public double decimal() {
			return decimal;
		}

		@Override
		public int position() {
			return UAR_MIN;
		}

		@Override
		public String value() {
			return String.valueOf(decimal);
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(this)
							.add("hex", hex)
							.add("decimal", decimal)
							.toString();
		}
		
		private void convertToDecimalAndCalculate(String hexValue) {
			List<String> split = Splitter.fixedLength(2).splitToList(hexValue);
			String inverted = split.get(1) + split.get(0);
			
			double converted = Integer.parseInt(inverted, 16);
			
			decimal = converted / 10;
		}
	}

	public static class Max implements Reportable {

		private String hex;
		private double decimal;

		public Max(String hexValue) {
			hex = hexValue;
			convertToDecimalAndCalculate(hexValue);
		}

		@Override
		public String header() {
			return "Uar max.";
		}
		
		public String hex() {
			return hex;
		}

		public double decimal() {
			return decimal;
		}

		@Override
		public int position() {
			return UAR_MAX;
		}

		@Override
		public String value() {
			return String.valueOf(decimal);
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(this)
							.add("hex", hex)
							.add("decimal", decimal)
							.toString();
		}
		
		private void convertToDecimalAndCalculate(String hexValue) {
			List<String> split = Splitter.fixedLength(2).splitToList(hexValue);
			String inverted = split.get(1) + split.get(0);
			
			double converted = Integer.parseInt(inverted, 16);
			
			decimal = converted / 10;
		}
	}
}

