package br.com.agsolve.parser.domain.userinput;

import com.google.common.base.Objects;

public class RawInputLine {

	static final String REPORT_LINE_START = "---";
	private static final int REPORTING_LINE_LENGTH = 537;
	
	private String line;

	public RawInputLine(String line) {
		this.line = line;
	}

	public boolean starts(String with) {
		return this.line.startsWith(with);
	}

	public String content() {
		return line;
	}
	
	public boolean isGoodForReporting() {
		return starts(REPORT_LINE_START) && line.length() == REPORTING_LINE_LENGTH;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("line", line)
						.toString();
	}
}
