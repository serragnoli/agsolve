package br.com.agsolve.parser.domain.file;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.common.base.Objects;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;

import br.com.agsolve.parser.domain.userinput.RawInputLine;

public class RawReportingLine {

	private static final Logger LOGGER = Logger.getLogger(RawReportingLine.class);
	private static final String START_OF_DATA_INDICATOR = ":";
	private static final String HEX_ZERO_LINE = Strings.repeat("F", 52);
	
	private List<TransposedLine> transposedLines = new ArrayList<>();
	private String line;

	public RawReportingLine(RawInputLine inputLine) {
		this.line = inputLine.content();
	}

	public String show() {
		return line;
	}

	public List<TransposedLine> transpose(int lineBreak) {
		LOGGER.info("Entering: transpose(");
		LOGGER.debug("Parameters: " + lineBreak);

		int startingPointInclusive = line.indexOf(START_OF_DATA_INDICATOR) + 1;

		String cleanLine = line.substring(startingPointInclusive);
		List<String> split = Splitter.fixedLength(lineBreak)
										.splitToList(cleanLine);

		for (String splitLine : split) {
			if (splitLine.length() == lineBreak && !HEX_ZERO_LINE.equals(splitLine)) {
				TransposedLine transposedLine = new TransposedLine(splitLine);
				transposedLines.add(transposedLine);
			}
		}

		LOGGER.info("Exiting: transpose()");
		LOGGER.debug("Returning: " + transposedLines);
		return transposedLines;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("line", line)
						.add("transposedLines", transposedLines)
						.toString();
	}

}
