package br.com.agsolve.parser.domain.analysis;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.common.base.Objects;

import br.com.agsolve.parser.domain.file.TransposedLine;
import br.com.agsolve.parser.domain.file.TransposedLines;

public class Analysis {

	private static final Logger LOG = Logger.getLogger(Analysis.class);
	
	private List<AnalysisLine> analysisLines = new ArrayList<>();

	public Analysis(TransposedLines transposedLines) {
		addLines(transposedLines);
	}

	public int totalLines() {
		return analysisLines.size();
	}

	public boolean isEmpty() {
		return analysisLines.isEmpty();
	}

	public AnalysisLine line(int number) {
		return analysisLines.get(number);
	}

	private void addLines(TransposedLines transposed) {
		LOG.debug("Entering: addLines -parameters " + transposed);
		
		for (TransposedLine line : transposed.lines()) {
			AnalysisLine analysisLine = new AnalysisLine(line);
			analysisLines.add(analysisLine);
		}
		
		LOG.debug("Exiting: addLines");
	}

	public void parseDates() {
		for (AnalysisLine line : analysisLines) {
			line.parseDate();
		}
	}

	public void parseBattery() {
		for (AnalysisLine line : analysisLines) {
			line.parseBattery();
		}
	}

	public void parseInternalTemperature() {
		for (AnalysisLine line : analysisLines) {
			line.parseInternalTemperature();
		}
	}

	public void parseId() {
		for (AnalysisLine line : analysisLines) {
			line.parseId();
		}
	}

	public void parseTar() {
		for (AnalysisLine line : analysisLines) {
			line.parseTar();
		}
	}

	public void parseUar() {
		for (AnalysisLine line : analysisLines) {
			line.parseUar();
		}
	}
	
	public void parseRadPar() {
		for (AnalysisLine line : analysisLines) {
			line.parseRadPar();
		}
	}
	
	public void parsePrecipitation() {
		for (AnalysisLine line : analysisLines) {
			line.parsePrecipitation();
		}
	}
	
	public List<AnalysisLine> lines() {
		return analysisLines;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("analysisLines", analysisLines)
						.toString();
	}

	// Unit test only
	void setAnalysisLines(List<AnalysisLine> analysisLines) {
		this.analysisLines = analysisLines;
	}
}
