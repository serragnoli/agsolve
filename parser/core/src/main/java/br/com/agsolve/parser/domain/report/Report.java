package br.com.agsolve.parser.domain.report;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;

import br.com.agsolve.parser.domain.analysis.Reportable;

public class Report {

	static final Report EMPTY = new Report();
	
	private List<ReportLine> lines = new ArrayList<>();
	private HeaderLine header = HeaderLine.EMPTY;

	private Report(){}
	
	public Report(List<List<Reportable>> collatedReportables) {
		checkArgument(collatedReportables.size() > 0, "There is no data to generate the report");
		
		header = new HeaderLine(collatedReportables.get(0));
		
		for (List<Reportable> reportable : collatedReportables) {
			ReportLine reportLine = new ReportLine(reportable);
			lines.add(reportLine);
		}
	}

	public String outputAsTabSeparated() {
		StringBuilder builder = new StringBuilder(header.outputAsTabSeparated());
		
		for (Printable printable : lines) {
			builder.append(printable.outputAsTabSeparated());
		}
		
		return builder.toString();
	}

	public HeaderLine header() {
		return header;
	}
	
	public int totalLines() {
		return lines.size();
	}

	public boolean isEmpty() {
		return lines.isEmpty();
	}
}
