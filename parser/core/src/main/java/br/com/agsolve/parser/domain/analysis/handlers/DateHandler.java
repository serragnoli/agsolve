package br.com.agsolve.parser.domain.analysis.handlers;

import org.apache.log4j.Logger;

import br.com.agsolve.parser.domain.analysis.Analysis;


class DateHandler extends Handler {

	private static final Logger LOG = Logger.getLogger(DateHandler.class);

	@Override
	void performStep(Analysis analysis) {
		LOG.info("Entering: performStep");
		LOG.debug("Parameter: " + analysis) ;

		analysis.parseDates();
		
		LOG.info("Exiting: performStep");
	}
}
