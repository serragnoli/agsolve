package br.com.agsolve.parser.domain.report;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.agsolve.parser.domain.analysis.Reportable;

public class ReportableSorter {

	private Comparator<Reportable> comparator;

	public ReportableSorter(Comparator<Reportable> comparator) {
		this.comparator = comparator;
	}

	public List<Reportable> sort(List<Reportable> reportables) {
		List<Reportable> sorted = new ArrayList<>(reportables);
		
		Collections.sort(sorted, comparator);
		
		return sorted;
	}
}
