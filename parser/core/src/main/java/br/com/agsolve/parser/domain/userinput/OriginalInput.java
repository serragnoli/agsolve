package br.com.agsolve.parser.domain.userinput;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.common.base.Objects;

public class OriginalInput {

	private static final Logger LOG = Logger.getLogger(OriginalInput.class);
	
	private List<RawInputLine> rawInputLines = new ArrayList<>();
	private List<String> rawLines;
	private Map<String, String> options;
	private int lineBreakPosition;

	public OriginalInput(Map<Object, Object> userParams) {
		validateParams(userParams);
		wrapLines();
	}

	private void wrapLines() {
		for (String rawLine : rawLines) {
			rawInputLines.add(new RawInputLine(rawLine));
		}
	}

	public int numberOfInputLines() {
		return rawInputLines.size();
	}

	public int lineBreak() {
		return lineBreakPosition;
	}

	public List<RawInputLine> linesRead() {
		return rawInputLines;
	}

	@SuppressWarnings("unchecked")
	private void validateParams(Map<Object, Object> userParams) {
		LOG.debug("Entering: validateParams -parameters " + userParams);
		
		rawLines = (List<String>) userParams.get("linesRead");

		if (null == rawLines || rawLines.isEmpty()) {
			throw new IllegalArgumentException("Lines of the uploaded cannot be identified");
		}

		options = (Map<String, String>) userParams.get("userOptions");
		if(null == options) {
			throw new IllegalArgumentException("You need to supply aat least the line should break position");
		}
		
		lineBreakPosition = Integer.parseInt(options.get("lineBreak"));
		
		LOG.debug("Exiting: validateParams");
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("rawInputLines", rawInputLines)
						.add("rawLines", rawLines)
						.add("options", options)
						.add("lineBreakPosition", lineBreakPosition)
						.toString();
	}
}
