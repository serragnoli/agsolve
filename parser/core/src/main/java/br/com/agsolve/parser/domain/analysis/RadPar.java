package br.com.agsolve.parser.domain.analysis;

import static br.com.agsolve.parser.domain.analysis.OrderingConstants.RAD_PAR;

import java.util.List;

import com.google.common.base.Objects;
import com.google.common.base.Splitter;

public class RadPar implements Reportable {

	private String hex;
	private double decimal;

	public RadPar(String hexValue) {
		hex = hexValue;
		convertoToDecimalAndCalculate(hexValue);
	}

	@Override
	public String header() {
		return "RadPar";
	}
	
	public String hex() {
		return hex;
	}

	public double decimal() {
		return decimal;
	}

	@Override
	public int position() {
		return RAD_PAR;
	}

	@Override
	public String value() {
		return String.valueOf(decimal);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("hex", hex)
						.add("decimal", decimal)
						.toString();
	}
	
	private void convertoToDecimalAndCalculate(String hexValue) {
		List<String> split = Splitter.fixedLength(2).splitToList(hexValue);
		String inverted = split.get(1) + split.get(0);
		
		double converted = Integer.parseInt(inverted, 16);
		
		decimal = converted / 10;
	}
}
