package br.com.agsolve.parser.domain.file;

import org.apache.log4j.Logger;

import br.com.agsolve.parser.domain.userinput.OriginalInput;

public class FileBO {

	private static final Logger LOGGER = Logger.getLogger(FileBO.class);

	public TransposedLines filterReportData(OriginalInput originalInput) {
		LOGGER.info("Entering: filterReportData");
		LOGGER.debug("Parameters: " + originalInput);

		RawReportLines rawReportLines = new RawReportLines(originalInput);
		TransposedLines transposedLines = rawReportLines.transposeReportingLines();

		LOGGER.info("Exiting: filterReportData");
		LOGGER.debug("Return value: " + transposedLines);
		return transposedLines;
	}
}
