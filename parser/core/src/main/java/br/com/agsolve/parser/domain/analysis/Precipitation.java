package br.com.agsolve.parser.domain.analysis;

import static br.com.agsolve.parser.domain.analysis.OrderingConstants.PRECIPITATION;

import java.util.List;

import com.google.common.base.Objects;
import com.google.common.base.Splitter;

public class Precipitation implements Reportable {

	private String hex;
	private double decimal;

	public Precipitation(String hexValue) {
		hex = hexValue;
		convertToDecimal(hexValue);
	}
	
	@Override
	public String header() {
		return "Precipitation";
	}

	public String hex() {
		return hex;
	}

	public double decimal() {
		return decimal;
	}

	@Override
	public int position() {
		return PRECIPITATION;
	}

	@Override
	public String value() {
		return String.valueOf(decimal);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("hex", hex)
						.add("decimal", decimal)
						.toString();
	}
	
	private void convertToDecimal(String hexValue) {
		List<String> split = Splitter.fixedLength(2).splitToList(hexValue);
		String inverted = split.get(1) + split.get(0);
		
		double converted = Integer.parseInt(inverted, 16);
		
		decimal = converted / 10;
	}
}
