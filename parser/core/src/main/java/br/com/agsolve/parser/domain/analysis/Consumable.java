package br.com.agsolve.parser.domain.analysis;

import com.google.common.base.Objects;

public class Consumable {

	private static final int DATE_LENGTH = 12;
	private static final int BATTERY_LENGTH = 2;
	private static final int INTERNAL_TEMPERATURE_LENGTH = 2;
	private static final int ID_LENGTH = 4;
	private static final int TAR_LENGTH = 12;
	private static final int UAR_LENGTH = 12;
	private static final int RAD_PAR_LENGTH = 4;
	private static final int PRECIPITATION_LENGTH = 4;

	private StringBuilder value;

	public Consumable(String value) {
		this.value = new StringBuilder(value);
	}

	public String display() {
		return value.toString();
	}

	public String consumeDate() {
		return consume(DATE_LENGTH);
	}

	public String consumeBattery() {
		return consume(BATTERY_LENGTH);
	}

	public String consumeInternalTemperature() {
		return consume(INTERNAL_TEMPERATURE_LENGTH);
	}

	public String consumeId() {
		return consume(ID_LENGTH);
	}

	public String consumeTar() {
		return consume(TAR_LENGTH);
	}

	public String consumeUar() {
		return consume(UAR_LENGTH);
	}

	public String consumeRadPar() {
		return consume(RAD_PAR_LENGTH);
	}

	public String consumePrecipitation() {
		return consume(PRECIPITATION_LENGTH);
	}

	private String consume(int dateLength) {
		String consumed = value.substring(0, dateLength);
		value.delete(0, dateLength);

		return consumed;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
						.add("value", value)
						.toString();
	}
}
