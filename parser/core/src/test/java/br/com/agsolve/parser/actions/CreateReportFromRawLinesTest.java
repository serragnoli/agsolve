package br.com.agsolve.parser.actions;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.AnalysisBO;
import br.com.agsolve.parser.domain.file.FileBO;
import br.com.agsolve.parser.domain.file.TransposedLines;
import br.com.agsolve.parser.domain.report.ReportBO;
import br.com.agsolve.parser.domain.userinput.OriginalInput;
import br.com.agsolve.parser.domain.userinput.UserInputBO;

public class CreateReportFromRawLinesTest {

	private static final Map<Object, Object> USER_PARAMS = new HashMap<>();
	
	private CreateReportFromRawLines createReportFromRawLines;	
	private FileBO fileBO;
	private AnalysisBO analysisBO;
	private ReportBO reportBO;

	private UserInputBO userInputBO;

	
	@Before public void 
	setup() {
		userInputBO = mock(UserInputBO.class);
		fileBO = mock(FileBO.class);
		analysisBO = mock(AnalysisBO.class);
		reportBO = mock(ReportBO.class);
		createReportFromRawLines = new CreateReportFromRawLines(userInputBO, fileBO, analysisBO, reportBO);
		createReportFromRawLines.parseRawLines(USER_PARAMS);
	}
	
	@Test public void 
	should_delegate_to_user_input_service() {
		verify(userInputBO).treat(USER_PARAMS);
	}
	
	@Test public void 
	should_delegate_to_file_service() {
		verify(fileBO).filterReportData(any(OriginalInput.class));
	}

	@Test public void 
	should_delegate_to_analysis_service() {
		verify(analysisBO).generateAnalysis(any(TransposedLines.class));
	}
	
	@Test public void 
	should_delegate_to_report_service() {
		verify(reportBO).prepare(any(Analysis.class));
	}
}
