package br.com.agsolve.parser.domain.analysis;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.domain.file.TransposedLines;

public class AnalysisBOTest {
	
	private AnalysisGenerator analysisGenerator;
	private TransposedLines transposedLines;
	private Analysis analysis;
	private AnalysisBO analysisBO;
	
	@Before public void
	setup() {
		analysis = mock(Analysis.class);
		analysisGenerator = mock(AnalysisGenerator.class);
		analysisBO = new AnalysisBO(analysisGenerator);
		analysisBO.setAnalysis(analysis);
	}

	@Test public void 
	should_delegate_to_generator_to_obtain_actual_report() {
		analysisBO.generateAnalysis(transposedLines);
		
		verify(analysisGenerator).generate(any(TransposedLines.class));
	}
	
	@Test public void 
	should_return_collation_of_reportables() {
		analysis = analysisBO.analysis();
		
		assertThat(analysis, is(notNullValue()));
	}
	
	@Test public void 
	should_have_toString_overridden() {
		assertThat(analysisBO.toString(), not(containsString("@")));
	}
}
