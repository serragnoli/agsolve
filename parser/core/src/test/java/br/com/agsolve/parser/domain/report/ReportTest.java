package br.com.agsolve.parser.domain.report;

import static br.com.agsolve.parser.ParametersBuilder.analysisWith;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.com.agsolve.parser.ParametersBuilder;
import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.Reportable;

@SuppressWarnings({"unused", "boxing"})
public class ReportTest {

	private static String expectedReportOutput;
	private List<List<Reportable>> sortedReportables;
	
	@BeforeClass public static void
	prepare() {
		expectedReportOutput = ParametersBuilder.reportLinesWithHeader(2);
	}
	
	@Before public void
	setup() {
		ReportableSorter sorter = new ReportableSorter(new ReportableComparator());		
		sortedReportables = new ArrayList<>();
		Analysis analysis = analysisWith(2);
		
		for(int i = 0; i < 2; i++) {
			List<Reportable> reportables = analysis.line(i).reportables();
			List<Reportable> sorted = sorter.sort(reportables);
			sortedReportables.add(sorted);
		}
	}
	
	@Test(expected = IllegalArgumentException.class) public void 
	should_throw_exception_when_analysis_is_empty() {
		new Report(new ArrayList<List<Reportable>>());
	}
	
	@Test public void 
	should_generate_header() {
		Report report = new Report(sortedReportables);
		
		assertThat(report.header(), is(notNullValue()));
	}
	
	@Test public void 
	should_generate_lines() {
		Report report = new Report(sortedReportables);
		
		assertThat(report.totalLines(), is(2));
	}
	
	@Test public void 
	should_output_as_tab_separated_text() {
		Report report = new Report(sortedReportables);
		
		assertThat(report.outputAsTabSeparated(), is(expectedReportOutput));
	}
	
	@Test public void 
	should_have_elements_when_reportables_exist() {
		Report report = new Report(sortedReportables);
		
		assertThat(report.isEmpty(), is(false));
	}
	
	@Test public void 
	empty_report_should_be_empty() {
		Report report = Report.EMPTY;
		
		assertThat(report.isEmpty(), is(true));
	}
	
	@Test public void 
	should_contain_message_that_the_report_is_empty() {
		Report report = Report.EMPTY;
		
		assertThat(report.outputAsTabSeparated(), is("No data to be displayed in this report\n"));
	}
}
