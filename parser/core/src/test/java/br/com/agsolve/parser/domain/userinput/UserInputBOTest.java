package br.com.agsolve.parser.domain.userinput;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import br.com.agsolve.parser.ParametersBuilder;

public class UserInputBOTest {

	private static Map<Object, Object> PERFECT_PARAM_WITH_TWO_REPORTABLE_LINES;

	@BeforeClass public static void 
	prepare() {
		PERFECT_PARAM_WITH_TWO_REPORTABLE_LINES = new ParametersBuilder().withLineBreakPosition("52")
																						.withHeaders()
																						.withValidLines(2)
																						.build();
	}
	
	@Test public void 
	should_return_parameters_wrapper() {		
		UserInputBO userInput = new UserInputBO();
		OriginalInput inputWrapper = userInput.treat(PERFECT_PARAM_WITH_TWO_REPORTABLE_LINES);
		
		assertThat(inputWrapper, is(notNullValue()));
	}
}
