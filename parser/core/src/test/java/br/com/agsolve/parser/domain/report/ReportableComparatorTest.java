package br.com.agsolve.parser.domain.report;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.domain.analysis.Battery;
import br.com.agsolve.parser.domain.analysis.DateTime;
import br.com.agsolve.parser.domain.analysis.Id;
import br.com.agsolve.parser.domain.analysis.InternalTemperature;
import br.com.agsolve.parser.domain.analysis.Precipitation;
import br.com.agsolve.parser.domain.analysis.RadPar;
import br.com.agsolve.parser.domain.analysis.Reportable;
import br.com.agsolve.parser.domain.analysis.Tar;
import br.com.agsolve.parser.domain.analysis.Uar;

public class ReportableComparatorTest {
	
	private static final String DATE_TEXT = "1002011010000";
	private static final String HEX = "FFFF";
	private static final String EXTENDED_HEX = "FFFFFFFFFFFF";
	
	private Comparator<Reportable> comparator = new ReportableComparator();
	private List<Reportable> shuffled;
	
	@Before public void
	setup() {
		shuffled = new ArrayList<>();
		shuffled.add(new Tar(EXTENDED_HEX).min());
		shuffled.add(new Battery(HEX));
		shuffled.add(new Uar(EXTENDED_HEX).average());
		shuffled.add(new Tar(EXTENDED_HEX).max());
		shuffled.add(new Uar(EXTENDED_HEX).min());
		shuffled.add(new Precipitation(HEX));
		shuffled.add(new DateTime(DATE_TEXT));
		shuffled.add(new Tar(EXTENDED_HEX).average());
		shuffled.add(new InternalTemperature(HEX));
		shuffled.add(new Uar(EXTENDED_HEX).max());
		shuffled.add(new RadPar(HEX));
		shuffled.add(new Id(HEX));
	}
		
	@Test public void 
	datetime_should_be_1st() {
		Collections.sort(shuffled, comparator);
		
		assertThat(shuffled.get(0), is(instanceOf(DateTime.class)));
	}
	
	@Test public void 
	battery_should_be_2nd() {
		Collections.sort(shuffled, comparator);
		
		assertThat(shuffled.get(1), is(instanceOf(Battery.class)));
	}
	
	@Test public void 
	internal_temperature_should_be_3rd() {
		Collections.sort(shuffled, comparator);
		
		assertThat(shuffled.get(2), is(instanceOf(InternalTemperature.class)));
	}
	
	@Test public void 
	id_should_be_4th() {
		Collections.sort(shuffled, comparator);
		
		assertThat(shuffled.get(3), is(instanceOf(Id.class)));
	}
	
	@Test public void 
	tar_average_should_be_5th() {
		Collections.sort(shuffled, comparator);
		
		assertThat(shuffled.get(4), is(instanceOf(Tar.Average.class)));
	}
	
	@Test public void 
	tar_min_should_be_6th() {
		Collections.sort(shuffled, comparator);
		
		assertThat(shuffled.get(5), is(instanceOf(Tar.Min.class)));
	}
	
	@Test public void 
	tar_max_should_be_7th() {
		Collections.sort(shuffled, comparator);
		
		assertThat(shuffled.get(6), is(instanceOf(Tar.Max.class)));
	}
	
	@Test public void 
	uar_average_should_be_8th() {
		Collections.sort(shuffled, comparator);
		
		assertThat(shuffled.get(7), is(instanceOf(Uar.Average.class)));
	}

	@Test public void 
	uar_min_should_be_9th() {
		Collections.sort(shuffled, comparator);
		
		assertThat(shuffled.get(8), is(instanceOf(Uar.Min.class)));
	}

	@Test public void 
	uar_max_should_be_10th() {
		Collections.sort(shuffled, comparator);
		
		assertThat(shuffled.get(9), is(instanceOf(Uar.Max.class)));
	}
	
	@Test public void 
	radPar_should_be_11th() {
		Collections.sort(shuffled, comparator);
		
		assertThat(shuffled.get(10), is(instanceOf(RadPar.class)));
	}

	@Test public void 
	precipitation_should_be_12th() {
		Collections.sort(shuffled, comparator);
		
		assertThat(shuffled.get(11), is(instanceOf(Precipitation.class)));
	}

}
