package br.com.agsolve.parser.domain.analysis;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("boxing")
public class RadParTest {

	private static final String HEX_OF_29 = "1D00";
	private RadPar radPar;
	
	@Before public void
	setup() {
		radPar = new RadPar(HEX_OF_29);		
	}
	
	@Test public void 
	should_display_header() {
		String header = radPar.header();
		
		assertThat(header, is(notNullValue(String.class)));
	}
	
	@Test public void 
	should_record_hex() {
		assertThat(radPar.hex(), is(HEX_OF_29));
	}
	
	@Test public void 
	should_convert_from_hex_to_decimal() {
		assertThat(radPar.decimal(), is(2.9));
	}
	
	@Test public void 
	should_output_value_for_printing() {
		assertThat(radPar.value(), is("2.9"));
	}
}
