package br.com.agsolve.parser.domain.userinput;

import static br.com.agsolve.parser.domain.userinput.RawInputLine.REPORT_LINE_START;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.ParametersBuilder;

@SuppressWarnings("boxing")
public class RawInputLineTest {

	private static final String GOOD_FOR_REPORTING = ParametersBuilder.valid(1);
	private static final String BAD_FOR_REPORTING = ParametersBuilder.footers(1);
	private RawInputLine line;
	
	@Before public void
	setup() {
		line = new RawInputLine(GOOD_FOR_REPORTING);
	}
	
	@Test public void 
	should_retain_content_of_line() {	
		assertThat(line.content(), is(GOOD_FOR_REPORTING));
	}
	
	@Test public void 
	should_match_start_of_line() {
		boolean result = line.starts(REPORT_LINE_START);
		
		assertThat(result, is(true));
	}
	
	@Test public void 
	should_not_match_start_of_the_line() {
		String noMatch = "99";
		
		boolean result = line.starts(noMatch);
		
		assertThat(result, is(false));
	}
	
	@Test public void 
	should_say_its_good_for_reporting() {
		line = new RawInputLine(BAD_FOR_REPORTING);
		
		boolean goodForReporting = line.isGoodForReporting();
		
		assertThat(goodForReporting, is(false));
	}
}
