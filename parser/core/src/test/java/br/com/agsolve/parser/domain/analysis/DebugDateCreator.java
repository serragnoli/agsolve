package br.com.agsolve.parser.domain.analysis;

import java.util.Calendar;
import java.util.Date;
import static java.util.Calendar.YEAR;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.MILLISECOND;

//TODO fabio 2 Aug 2014 17:03:01: Remove this class as it is not being used anywhere else
public class DebugDateCreator {

	static Date createDate(int year, int month, int day, int hour, int min, int sec) {
		Calendar cal = Calendar.getInstance();
		cal.set(YEAR, year);
		cal.set(MONTH, month);
		cal.set(DAY_OF_MONTH, day);
		cal.set(HOUR_OF_DAY, hour);
		cal.set(MINUTE, min);
		cal.set(SECOND, sec);
		cal.set(MILLISECOND, 0);

		return cal.getTime();
	}
}
