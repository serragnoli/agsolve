package br.com.agsolve.parser.domain.file;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import br.com.agsolve.parser.ParametersBuilder;
import br.com.agsolve.parser.domain.userinput.OriginalInput;

@SuppressWarnings("boxing")
public class RawReportLinesTest {

	private static Map<Object, Object> oneReportingLineFile;
	private static Map<Object, Object> threeReportingLineFile;

	@BeforeClass public static void 
	prepare() {
		oneReportingLineFile = new ParametersBuilder().withDefaultLineBreakPosition()
														.withHeaders()
														.withValidLines(1)
														.withFooters()
														.build();
		threeReportingLineFile = new ParametersBuilder().withDefaultLineBreakPosition()
																	.withHeaders()
																	.withValidLines(3)
																	.withFooters()
																	.build();
	}
	
	@Test public void 
	should_keep_the_only_one_reporting_line() {
		OriginalInput oneLineFileInput = new OriginalInput(oneReportingLineFile);
		
		RawReportLines rawReportLines = new RawReportLines(oneLineFileInput);
		
		assertThat(rawReportLines.size(), is(1));
	}
	
	@Test public void 
	should_keep_the_three_reporting_lines() {
		OriginalInput threeLinesFileInput = new OriginalInput(threeReportingLineFile);
		RawReportLines rawReportLines = new RawReportLines(threeLinesFileInput);
		
		assertThat(rawReportLines.size(), is(3));
	}
	
	@Test public void 
	should_return_transposed_reporting_lines_identified() {
		OriginalInput oneLineFileInput = new OriginalInput(oneReportingLineFile);
		RawReportLines reportLines = new RawReportLines(oneLineFileInput);

		TransposedLines transposedLines = reportLines.transposeReportingLines();
		
		assertThat(transposedLines.size(), is(10));
	}
	
	@Test public void 
	should_have_toString_overridden() {
		OriginalInput oneLineFileInput = new OriginalInput(oneReportingLineFile);
		RawReportLines rawReportLines = new RawReportLines(oneLineFileInput);
		
		assertThat(rawReportLines.toString(), not(containsString("@")));
	}
}
