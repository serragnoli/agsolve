package br.com.agsolve.parser.domain.analysis.handlers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;

import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.handlers.Handler;
import br.com.agsolve.parser.domain.analysis.handlers.UarHandler;

public class UarHandlerTest {

	private Handler handler = new UarHandler();
	private Analysis analysis = mock(Analysis.class);

	@Test public void 
	should_invoke_report_to_extract_uar() {
		handler.handle(analysis);
		
		verify(analysis).parseUar();
	}
}
