package br.com.agsolve.parser.domain.file;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.ParametersBuilder;
import br.com.agsolve.parser.domain.userinput.RawInputLine;

//TODO fabio 10 Aug 2014 20:03:26: This whole test is too verbose to set up
@SuppressWarnings("boxing")
public class RawReportingLineTest {

	private static final String RAW_VALID_LINE = ParametersBuilder.valid(0);
	private static final String RAW_LINE_WITH_MANY_FFFF = ParametersBuilder.valid(17);
	private static final int DEFAULT_LINE_BREAK = ParametersBuilder.defaultLineBreak();
	private RawInputLine rawInputLine;
	private RawReportingLine rawReportingLine;
	
	@Before public void
	setup() {
		rawInputLine = new RawInputLine(RAW_VALID_LINE);		
		rawReportingLine = new RawReportingLine(rawInputLine);
	}

	@Test public void 
	should_copy_the_content_of_the_raw_input_line() {
		assertThat(rawReportingLine.show(), is(RAW_VALID_LINE));
	}
	
	@Test public void 
	should_generate_ten_transposed_lines() {
		List<TransposedLine> transposedLines = rawReportingLine.transpose(DEFAULT_LINE_BREAK);
		
		assertThat(transposedLines.size(), is(10));
	}
	
	@Test public void 
	first_transposed_line_should_be_as_expected() {
		List<TransposedLine> transposedLines = rawReportingLine.transpose(DEFAULT_LINE_BREAK);
		
		TransposedLine firstTransposedLine = transposedLines.get(0);
		assertThat(firstTransposedLine.display(), is("1402151210002F910100E100E100E2009F039E03A1031D000000"));
	}
	
	@Test public void 
	last_transposed_line_should_be_as_expected() {
		List<TransposedLine> transposedLines = rawReportingLine.transpose(DEFAULT_LINE_BREAK);

		int lastIndex = transposedLines.size() - 1;
		TransposedLine lastTransposedLine = transposedLines.get(lastIndex);
		assertThat(lastTransposedLine.display(), is("14021513400047990100050103010A017B037703800377000000"));
	}
	
	@Test public void 
	should_ignore_lines_that_are_totally_filled_with_hex_ffff() {
		rawInputLine = new RawInputLine(RAW_LINE_WITH_MANY_FFFF);
		rawReportingLine = new RawReportingLine(rawInputLine);
		
		List<TransposedLine> transposedLines = rawReportingLine.transpose(DEFAULT_LINE_BREAK);
		
		assertThat(transposedLines.size(), is(5));
	}
	
	@Test public void 
	should_have_toString_overridden() {
		assertThat(rawReportingLine.toString(), not(containsString("@")));
	}
}
