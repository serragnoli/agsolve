package br.com.agsolve.parser.domain.report;

import static br.com.agsolve.parser.ParametersBuilder.analysisWith;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.BATTERY;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.DATE_TIME;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.ID;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.INTERNAL_TEMPERATURE;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.PRECIPITATION;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.RAD_PAR;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.TAR_AVERAGE;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.TAR_MAX;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.TAR_MIN;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.UAR_AVERAGE;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.UAR_MAX;
import static br.com.agsolve.parser.domain.analysis.OrderingConstants.UAR_MIN;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Comparator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.domain.analysis.Battery;
import br.com.agsolve.parser.domain.analysis.DateTime;
import br.com.agsolve.parser.domain.analysis.Id;
import br.com.agsolve.parser.domain.analysis.InternalTemperature;
import br.com.agsolve.parser.domain.analysis.Precipitation;
import br.com.agsolve.parser.domain.analysis.RadPar;
import br.com.agsolve.parser.domain.analysis.Reportable;
import br.com.agsolve.parser.domain.analysis.Tar;
import br.com.agsolve.parser.domain.analysis.Uar;

public class ReportableSorterTest {

	private static final List<Reportable> SHUFFLED = analysisWith(1).line(0).reportables();
	private ReportableSorter sorter;
	private List<Reportable> sorted;
	
	@Before public void
	setup() {
		Comparator<Reportable> comparator = new ReportableComparator();
		sorter = new ReportableSorter(comparator);
		sorted = sorter.sort(SHUFFLED);
	}
	
	@Test public void 
	should_have_datetime_in_order() {
		assertThat(sorted.get(DATE_TIME-1), is(instanceOf(DateTime.class)));
	}
	
	@Test public void 
	should_have_battery_in_order() {
		assertThat(sorted.get(BATTERY-1), is(instanceOf(Battery.class)));
	}
	
	@Test public void 
	should_have_internal_temperature_in_order() {
		assertThat(sorted.get(INTERNAL_TEMPERATURE-1), is(instanceOf(InternalTemperature.class)));
	}
	
	@Test public void 
	should_have_id_in_order() {
		assertThat(sorted.get(ID-1), is(instanceOf(Id.class)));
	}
	
	@Test public void 
	should_have_tar_avg_in_order() {
		assertThat(sorted.get(TAR_AVERAGE-1), is(instanceOf(Tar.Average.class)));
	}
	
	@Test public void 
	should_have_tar_min_in_order() {
		assertThat(sorted.get(TAR_MIN-1), is(instanceOf(Tar.Min.class)));
	}
	
	@Test public void 
	should_have_tar_max_in_order() {
		assertThat(sorted.get(TAR_MAX-1), is(instanceOf(Tar.Max.class)));
	}
	
	@Test public void 
	should_have_uar_avg_in_order() {
		assertThat(sorted.get(UAR_AVERAGE-1), is(instanceOf(Uar.Average.class)));
	}
	
	@Test public void 
	should_have_uar_min_in_order() {
		assertThat(sorted.get(UAR_MIN-1), is(instanceOf(Uar.Min.class)));
	}
	
	@Test public void 
	should_have_uar_max_in_order() {
		assertThat(sorted.get(UAR_MAX-1), is(instanceOf(Uar.Max.class)));
	}
	
	@Test public void 
	should_have_radpar_in_order() {
		assertThat(sorted.get(RAD_PAR-1), is(instanceOf(RadPar.class)));
	}
	
	@Test public void 
	should_have_precipitation_in_order() {
		assertThat(sorted.get(PRECIPITATION-1), is(instanceOf(Precipitation.class)));
	}
}
