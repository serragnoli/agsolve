package br.com.agsolve.parser.domain.analysis;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("boxing")
public class InternalTemperatureTest {

	private static final String HEX_TO_TWENTY_TWO_FIVE_CELSIUS = "91";
	private static final String HEX_TO_TWENTY_EIGHT_FIVE_CELSIUS = "9D";
	private InternalTemperature internalTemperature;

	@Before public void 
	setup() {
		internalTemperature = new InternalTemperature(HEX_TO_TWENTY_EIGHT_FIVE_CELSIUS);
	}

	@Test public void 
	should_display_header() {
		String header = internalTemperature.header();
		
		assertThat(header, is(notNullValue(String.class)));
	}
	
	@Test public void 
	should_record_hex_value() {
		assertThat(internalTemperature.hex(), is(HEX_TO_TWENTY_EIGHT_FIVE_CELSIUS));
	}
	
	@Test public void 
	should_convert_another_hex_to_celsius() {
		assertThat(internalTemperature.inCelsius(), is(28.5));
	}

	@Test public void 
	should_convert_hex_to_celsius() {
		internalTemperature = new InternalTemperature(HEX_TO_TWENTY_TWO_FIVE_CELSIUS);
		
		assertThat(internalTemperature.inCelsius(), is(22.5));
	}
	
	@Test public void 
	should_output_value_for_printing() {
		assertThat(internalTemperature.value(), is("28.5"));
	}
}
