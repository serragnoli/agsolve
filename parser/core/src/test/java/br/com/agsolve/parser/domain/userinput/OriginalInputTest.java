package br.com.agsolve.parser.domain.userinput;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.com.agsolve.parser.ParametersBuilder;

@SuppressWarnings({"unused", "boxing"})
public class OriginalInputTest {

	private static Map<Object, Object> noFileUploaded;
	private static Map<Object, Object> missingReadLineKey;
	private static Map<Object, Object> missingRawFileKey;
	private static Map<Object, Object> missingLineBreakKey;
	private static Map<Object, Object> missingOptionsKey;
	private static Map<Object, Object> typicalParamsWithFullFile;
	private static ParametersBuilder fullFileBuilder;
	private OriginalInput originalInput;

	@BeforeClass
	public static void prepare() {
		noFileUploaded = new ParametersBuilder().withDefaultLineBreakPosition()
												.withValidLines(0)
												.build();
		missingReadLineKey = new ParametersBuilder().withDefaultLineBreakPosition()
													.withValidLines(3)
													.noLinesReadParam()
													.build();
		missingRawFileKey = new ParametersBuilder().withDefaultLineBreakPosition()
													.withValidLines(0)
													.build();
		missingLineBreakKey = new ParametersBuilder().withValidLines(1)
														.noLineBreakPosition()
														.build();
		missingOptionsKey = new ParametersBuilder().noUserOptionsKey()
													.withHeaders()
													.withValidLines(1)
													.build();
		fullFileBuilder = new ParametersBuilder().withDefaultLineBreakPosition()
													.withValidLines(3)
													.withHeaders();
		typicalParamsWithFullFile = fullFileBuilder.build();
	}
	
	@Before public void 
	setup() {
		originalInput = new OriginalInput(typicalParamsWithFullFile); 
	}

	@Test(expected = IllegalArgumentException.class) public void 
	should_throw_exception_when_file_uploaded_have_zero_lines() {
		new OriginalInput(noFileUploaded);
	}

	@Test(expected = IllegalArgumentException.class) public void 
	should_throw_exception_when_user_parameter_is_missing_read_line_key() {
		new OriginalInput(missingReadLineKey);
	}

	@Test(expected = IllegalArgumentException.class) public void 
	should_throw_exception_when_user_parameter_is_missing_raw_report_lines_parameter() {
		new OriginalInput(missingRawFileKey);
	}

	@Test(expected = IllegalArgumentException.class) public void 
	should_throw_exception_when_no_line_break_key_in_parameters() {
		new OriginalInput(missingLineBreakKey);
	}
	
	@Test(expected = IllegalArgumentException.class) public void 
	should_throw_exception_when_parameter_is_missing_options_key() {
		new OriginalInput(missingOptionsKey);
	}

	 @Test public void
	 should_have_the_same_number_of_lines_of_the_uploaded_file() {
		 assertThat(originalInput.numberOfInputLines(), is(fullFileBuilder.linesInFullFile()));
	 }
	 
	 @Test public void 
	should_report_on_lines_break_specified_by_input() {
		assertThat(originalInput.lineBreak(), is(52));
	}
}
