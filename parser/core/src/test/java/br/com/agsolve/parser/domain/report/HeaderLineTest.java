package br.com.agsolve.parser.domain.report;

import static br.com.agsolve.parser.ParametersBuilder.analysisWith;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.com.agsolve.parser.ParametersBuilder;
import br.com.agsolve.parser.domain.analysis.Reportable;

@SuppressWarnings("boxing")
public class HeaderLineTest {

	private static String header;	
	private static String expectedHeader;
	private List<Reportable> sortedReportables;
	
	@BeforeClass public static void
	prepare() {
		expectedHeader = ParametersBuilder.reportingHeader();
		header = ParametersBuilder.reportingHeader();
	}
	
	@Before public void
	setup() {
		sortedReportables = analysisWith(1).lines().get(0).reportables();
		Collections.sort(sortedReportables, new ReportableComparator());
	}
	
	@Test public void 
	should_record_headers() {
		HeaderLine headerLine = new HeaderLine(sortedReportables);
		
		assertThat(headerLine.outputAsTabSeparated(), is(header));
	}
	
	@Test public void 
	should_output_header_as_tab_separated() {
		HeaderLine headerLine = new HeaderLine(sortedReportables);
		
		assertThat(headerLine.outputAsTabSeparated(), is(expectedHeader));
	}
	
	@Test public void 
	should_display_message_when_header_is_empty() {
		HeaderLine headerLine = HeaderLine.EMPTY;
		
		assertThat(headerLine.outputAsTabSeparated(), is("No data to be displayed in this report\n"));
	}
	
	@Test public void 
	should_append_carriage_return_at_the_end_of_the_line() {
		HeaderLine headerLine = new HeaderLine(sortedReportables);
		
		assertThat(headerLine.outputAsTabSeparated().endsWith("\n"), is(true));
	}
	
	@Test public void 
	should_have_toString_overridden() {
		HeaderLine headerLine = HeaderLine.EMPTY;
		
		assertThat(headerLine.toString(), not(containsString("@")));
	}
}
