package br.com.agsolve.parser.infra.spring;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.actions.CreateReportFromRawLines;
import br.com.agsolve.parser.domain.analysis.AnalysisBO;
import br.com.agsolve.parser.domain.file.FileBO;

public class CoreBeanConfigTest {

	private CoreBeanConfig config;
	
	@Before public void
	setup() {
		config = new CoreBeanConfig();
	}
	
	@Test public void 
	should_create_report_from_raw_lines() {
		CreateReportFromRawLines bean = config.createReportFromRawLines();
		
		assertThat(bean, is(notNullValue()));
	}
	
	@Test public void 
	should_create_userBO() {
		FileBO bean = config.fileBO();
		
		assertThat(bean, is(notNullValue()));
	}
	
	@Test public void 
	should_create_reportBO() {
		AnalysisBO bean = config.analysisBO();
		
		assertThat(bean, is(notNullValue()));
	}
}
