package br.com.agsolve.parser.actions;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import br.com.agsolve.parser.domain.file.TransposedLine;

public class TransposedLineTest {
	
	@Test public void 
	should_keep_line() {
		String anyLine = "anyLineText";
		
		TransposedLine transposedLine = new TransposedLine(anyLine);
		
		assertThat(transposedLine.display(), is(anyLine));
	}
}
