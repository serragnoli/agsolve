package br.com.agsolve.parser.domain.analysis.handlers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;

import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.handlers.InternalTemperatureHandler;

public class InternalTemperatureHandlerTest {

	private InternalTemperatureHandler tempHandler = new InternalTemperatureHandler();
	private Analysis analysis = mock(Analysis.class);
	
	@Test public void 
	should_invoke_report_to_extract_internal_temperature() {
		tempHandler.handle(analysis);

		verify(analysis).parseInternalTemperature();
	}
}
