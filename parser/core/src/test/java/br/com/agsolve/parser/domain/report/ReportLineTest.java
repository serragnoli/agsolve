package br.com.agsolve.parser.domain.report;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.ParametersBuilder;
import br.com.agsolve.parser.domain.analysis.Reportable;

public class ReportLineTest {

	private ReportLine reportLine;
	private List<Reportable> sortedReportables;
	private String expectedReportLine;
	
	@Before public void 
	setup() {
		sortedReportables = ParametersBuilder.analysisWith(1).line(0).reportables();
		expectedReportLine = ParametersBuilder.reportLines(1);
		
		Collections.sort(sortedReportables, new ReportableComparator());
		reportLine = new ReportLine(sortedReportables);
	}
	
	@Test public void 
	should_record_line() {
		assertThat(reportLine.outputAsTabSeparated(), is(expectedReportLine));
	}
	
	@Test public void 
	should_have_toString_overridden() {
		assertThat(reportLine.toString(), not(containsString("@")));
	}
}
