package br.com.agsolve.parser.domain.analysis;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("boxing")
public class IdTest {

	private static final String HEX_FOR_ID_ONE = "0100;";
	private static final String HEX_FOR_ID_TEN = "0A00";
	
	private Id id;
	
	@Before public void
	setup() {
		id = new Id(HEX_FOR_ID_ONE);
	}

	@Test public void 
	should_display_header() {
		String header = id.header();
		
		assertThat(header, is(notNullValue(String.class)));
	}
	
	@Test public void 
	should_convert_hex_to_id() {
		assertThat(id.decimal(), is(1));
	}
	
	@Test public void 
	should_record_hex_value() {
		assertThat(id.hex(), is(HEX_FOR_ID_ONE));
	}

	@Test public void 
	should_convert_another_hex_to_id() {
		id = new Id(HEX_FOR_ID_TEN);
		
		assertThat(id.decimal(), is(10));
	}
	
	@Test public void 
	should_output_value_for_printing() {
		assertThat(id.value(), is("1"));
	}
}
