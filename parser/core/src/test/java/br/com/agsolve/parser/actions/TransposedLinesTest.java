package br.com.agsolve.parser.actions;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.domain.file.TransposedLine;
import br.com.agsolve.parser.domain.file.TransposedLines;

@SuppressWarnings("boxing")
public class TransposedLinesTest {

	private List<TransposedLine> manyTransposedLines;
	private List<TransposedLine> moreTransposedLines;
	
	@Before public void
	setup() {
		manyTransposedLines = new ArrayList<>();
		manyTransposedLines.add(new TransposedLine("Line A"));
		manyTransposedLines.add(new TransposedLine("Line B"));
		
		moreTransposedLines = new ArrayList<>();
		moreTransposedLines.add(new TransposedLine("Line C"));
	}
	
	@Test public void 
	should_allow_adding_whole_lists() {
		TransposedLines transposedLines = new TransposedLines();
		
		transposedLines.addAll(manyTransposedLines);
		transposedLines.addAll(moreTransposedLines);
		
		assertThat(transposedLines.size(), is(3));
	}
}
