package br.com.agsolve.parser.domain.file;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.ParametersBuilder;
import br.com.agsolve.parser.domain.userinput.OriginalInput;

public class FileBOTest {

	private Map<Object, Object> parameters;
	private OriginalInput originalInput;

	@Before public void
	setup() {
		parameters = new ParametersBuilder().withDefaultLineBreakPosition()
											.withValidLines(1)
											.build();
		originalInput = new OriginalInput(parameters);
	}
	
	@Test public void 
	should_return_lines_transposed() {
		FileBO fileBO = new FileBO();
		TransposedLines transposed = fileBO.filterReportData(originalInput);

		assertThat(transposed, is(notNullValue()));
	}
}
