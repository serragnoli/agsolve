package br.com.agsolve.parser.domain.analysis;

import static br.com.agsolve.parser.ParametersBuilder.transposedLine;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.domain.file.TransposedLine;
import br.com.agsolve.parser.domain.file.TransposedLines;

@SuppressWarnings("boxing")
public class AnalysisTest {

	private TransposedLines transposedLines;
	private Analysis analysis;
	private AnalysisLine analysisLine1;
	private AnalysisLine analysisLine2;

	@Before public void
	setup() {
		transposedLines = mock(TransposedLines.class);
		analysisLine1 = mock(AnalysisLine.class);
		analysisLine2 = mock(AnalysisLine.class);
		
		analysis = new Analysis(transposedLines);
		analysis.setAnalysisLines(asList(analysisLine1, analysisLine2));
	}
	
	@Test public void 
	should_remember_all_transposed_lines() {
		assertThat(analysis.totalLines(), is(2));
	}
	
	@Test public void 
	should_be_able_to_retrieve_a_specific_line() {
		AnalysisLine line = analysis.line(0);
		
		assertThat(line, is(notNullValue()));
	}
	
	@Test public void 
	should_convert_transposed_lines_into_analysis_lines() {
		String transposedLineText = transposedLine(0);
		TransposedLine transposedLine = new TransposedLine(transposedLineText);
		transposedLines = new TransposedLines();
		transposedLines.addAll(asList(transposedLine));
		
		analysis = new Analysis(transposedLines);
		
		assertThat(analysis.totalLines(), is(1));
	}
	
	@Test public void 
	should_be_empty_when_transposed_lines_are_empty() {
		analysis = new Analysis(new TransposedLines());
		
		assertThat(analysis.isEmpty(), is(true));
	}
	
	@Test public void 
	should_have_elements_when_transposed_lines_have_elements() {
		assertThat(analysis.isEmpty(), is(false));
	}
	
	@Test public void 
	should_delegate_to_each_line_itself_to_parse_date() {
		analysis.parseDates();
		
		verify(analysisLine1).parseDate();
		verify(analysisLine2).parseDate();
	}
	
	@Test public void 
	should_delegate_to_each_line_itself_to_parse_battery() {
		analysis.parseBattery();
		
		verify(analysisLine1).parseBattery();
		verify(analysisLine2).parseBattery();
	}
	
	@Test public void 
	should_delegate_to_each_line_itself_to_parse_internal_temperature() {
		analysis.parseInternalTemperature();
		
		verify(analysisLine1).parseInternalTemperature();
		verify(analysisLine2).parseInternalTemperature();
	}
	
	@Test public void 
	should_delegate_to_each_line_itself_to_parse_id() {
		analysis.parseId();
		
		verify(analysisLine1).parseId();
		verify(analysisLine2).parseId();
	}
	
	@Test public void 
	should_delegate_to_each_line_itself_to_parse_tar() {
		analysis.parseTar();
		
		verify(analysisLine1).parseTar();
		verify(analysisLine2).parseTar();
	}
	
	@Test public void 
	should_delegate_to_each_line_itself_to_parse_uar() {
		analysis.parseUar();
		
		verify(analysisLine1).parseUar();
		verify(analysisLine2).parseUar();
	}
	
	@Test public void 
	should_delegate_to_each_line_itself_to_parse_rad_par() {
		analysis.parseRadPar();
		
		verify(analysisLine1).parseRadPar();
		verify(analysisLine2).parseRadPar();
	}
	
	@Test public void 
	should_delegate_to_each_line_itself_to_parse_precipitation() {
		analysis.parsePrecipitation();
		
		verify(analysisLine1).parsePrecipitation();
		verify(analysisLine2).parsePrecipitation();
	}
	
	@Test public void 
	should_collate_reportables_from_all_lines() {
		List<AnalysisLine> analysisLines = analysis.lines();
		
		assertThat(analysisLines.get(0), is(analysisLine1));
		assertThat(analysisLines.get(1), is(analysisLine2));
	}
}