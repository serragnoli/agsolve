package br.com.agsolve.parser.domain.analysis.handlers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.Test;

import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.handlers.DateHandler;
import br.com.agsolve.parser.domain.analysis.handlers.Handler;

public class HandlerTest {

	private Handler handler = new DateHandler();
	private Handler next = mock(Handler.class);
	private Analysis analysis = mock(Analysis.class);

	@Test public void 
	should_only_invoke_next_when_it_exists() {
		handler.add(next);
		
		handler.invokeNext(analysis);
		
		verify(next).handle(analysis);
	}
	
	@Test public void 
	should_not_try_to_invoke_next_when_it_is_inexistent_() {
		handler.invokeNext(analysis);
		
		verify(next, never()).handle(analysis);
	}
}
