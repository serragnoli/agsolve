package br.com.agsolve.parser.domain.analysis;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.ParametersBuilder;
import br.com.agsolve.parser.domain.file.TransposedLine;

public class AnalysisLineTest {

	private static final String ORIGINAL_LINE = ParametersBuilder.transposedLine(0);
	private static final TransposedLine TRANSPOSED_LINE = new TransposedLine(ORIGINAL_LINE);
	
	private AnalysisLine analysisLine;

	@Before public void
	setup() {
		analysisLine = new AnalysisLine(TRANSPOSED_LINE);
	}
	
	@Test public void 
	should_record_original_line() {
		assertThat(analysisLine.original(), is(ORIGINAL_LINE));
	}
	
	@Test public void 
	should_record_consumable_line() {
		assertThat(analysisLine.consumable(), is(instanceOf(Consumable.class)));
	}
	
	@Test public void 
	should_parse_date_as_expected() {
		analysisLine.parseDate();
		
		assertThat(analysisLine.dateTime(), is(instanceOf(DateTime.class)));
	}
	
	@Test public void 
	should_create_battery_from_consumable_line() {
		analysisLine.parseBattery();
		
		assertThat(analysisLine.battery(), is(instanceOf(Battery.class)));
	}
	
	@Test public void 
	should_create_internal_temperature_from_consumable_line() {
		analysisLine.parseInternalTemperature();
		
		assertThat(analysisLine.internalTemperature(), is(instanceOf(InternalTemperature.class)));
	}
	
	@Test public void 
	should_create_id_from_consumable_line() {
		analysisLine.parseId();
		
		assertThat(analysisLine.id(), is(instanceOf(Id.class)));
	}
	
	@Test public void 
	should_create_tar_from_consumable_line() {
		analysisLine.parseTar();
		
		assertThat(analysisLine.tar(), is(instanceOf(Tar.class)));
	}
	
	@Test public void 
	should_create_uar_from_consumable_line() {
		analysisLine.parseUar();
		
		assertThat(analysisLine.uar(), is(instanceOf(Uar.class)));
	}
	
	@Test public void 
	should_create_rad_par_from_consumable_line() {
		analysisLine.parseRadPar();
		
		assertThat(analysisLine.radPar(), is(instanceOf(RadPar.class)));
	}
	
	@Test public void 
	should_create_precipitation_from_consumable_line() {
		analysisLine.parsePrecipitation();
		
		assertThat(analysisLine.precipitation(), is(instanceOf(Precipitation.class)));
	}
}
