package br.com.agsolve.parser.domain.analysis.handlers;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.domain.analysis.handlers.BatteryHandler;
import br.com.agsolve.parser.domain.analysis.handlers.DateHandler;
import br.com.agsolve.parser.domain.analysis.handlers.Handler;
import br.com.agsolve.parser.domain.analysis.handlers.HandlersFactory;
import br.com.agsolve.parser.domain.analysis.handlers.IdHandler;
import br.com.agsolve.parser.domain.analysis.handlers.InternalTemperatureHandler;
import br.com.agsolve.parser.domain.analysis.handlers.PrecipitationHandler;
import br.com.agsolve.parser.domain.analysis.handlers.RadParHandler;
import br.com.agsolve.parser.domain.analysis.handlers.TarHandler;
import br.com.agsolve.parser.domain.analysis.handlers.UarHandler;

public class HandlersFactoryTest {
	
	private HandlersFactory factory;
	private Handler firstHandler;

	@Before public void
	setup() {
		factory = new HandlersFactory();
		firstHandler = factory.produceHandlers();
	}
	
	@Test public void 
	should_create_handlers() {
		assertThat(firstHandler, is(notNullValue()));
	}
	
	@Test public void 
	date_handler_should_be_the_first() {
		assertThat(firstHandler, is(instanceOf(DateHandler.class)));
	}
	
	@Test public void 
	battery_handler_should_be_the_second() {
		Handler secondHandler = firstHandler.next();
		
		assertThat(secondHandler, is(instanceOf(BatteryHandler.class)));
	}
	
	@Test public void 
	internal_temperature_handler_should_be_the_third() {
		Handler thirdHandler = firstHandler.next().next();
		
		assertThat(thirdHandler, is(instanceOf(InternalTemperatureHandler.class)));
	}
	
	@Test public void 
	id_handler_should_be_the_fourth() {
		Handler fourthHandler = firstHandler.next().next().next();
		
		assertThat(fourthHandler, is(instanceOf(IdHandler.class)));
	}
	
	@Test public void 
	tar_handler_should_be_the_fifth() {
		Handler fifthHandler = firstHandler.next().next().next().next();
		
		assertThat(fifthHandler, is(instanceOf(TarHandler.class)));
	}
	
	@Test public void 
	uar_handler_should_be_the_sixth() {
		Handler sixthHandler = firstHandler.next().next().next().next().next();
		
		assertThat(sixthHandler, is(instanceOf(UarHandler.class)));
	}
	
	@Test public void 
	rad_par_handler_should_be_the_seventh() {
		Handler seventhHandler = firstHandler.next().next().next().next().next().next();
		
		assertThat(seventhHandler, is(instanceOf(RadParHandler.class)));
	}
	
	@Test public void 
	precipitation_handler_should_be_the_eight() {
		Handler eighthHandler = firstHandler.next().next().next().next().next().next().next();
		
		assertThat(eighthHandler, is(instanceOf(PrecipitationHandler.class)));
	}
}
