package br.com.agsolve.parser.domain.analysis.handlers;

import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.handlers.DateHandler;

public class DateHandlerTest {

	private DateHandler dateHandler;
	private Analysis analysis;

	@Before public void
	setup() {
		analysis = Mockito.mock(Analysis.class);
		dateHandler = new DateHandler();		
	}
	
	@Test public void 
	should_invoke_report_to_parse_dates() {
		dateHandler.handle(analysis);
		
		verify(analysis).parseDates();
	}
}
