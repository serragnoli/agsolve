package br.com.agsolve.parser.domain.analysis;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("boxing")
public class BatteryTest {

	private static String HEX_FOR_DECIMAL_FOUR_SEVEN = "2F";
	private static String HEX_FOR_DECIMAL_SIX_SEVEN = "43";

	private Battery battery;
	
	@Before public void 
	setup() {
		battery = new Battery(HEX_FOR_DECIMAL_FOUR_SEVEN);
	}
	
	@Test public void 
	should_display_header() {
		String header = battery.header();
		
		assertThat(header, is(notNullValue(String.class)));
	}
	
	@Test public void 
	should_convert_hex_to_decimal() {
		assertThat(battery.decimal(), is(4.7));
	}
	
	@Test public void 
	should_convert_another_from_hex_to_decimal() {
		battery = new Battery(HEX_FOR_DECIMAL_SIX_SEVEN);
		
		assertThat(battery.decimal(), is(6.7));
	}
	
	@Test public void 
	should_keep_hex_value() {
		assertThat(battery.hex(), is(HEX_FOR_DECIMAL_FOUR_SEVEN));
	}
	
	@Test public void 
	should_output_value_for_printing() {
		assertThat(battery.value(), is("4.7"));
	}
}
