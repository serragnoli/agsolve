package br.com.agsolve.parser.domain.analysis;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("boxing")
public class PrecipitationTest {

	private static final String HEX_OF_06 = "0600";
	private static final String HEX_OF_24 = "1800";
	private Precipitation precipitation;

	@Before public void
	setup() {
		precipitation = new Precipitation(HEX_OF_06);
	}

	@Test public void 
	should_display_header() {
		String header = precipitation.header();
		
		assertThat(header, is(notNullValue(String.class)));
	}
	
	@Test public void 
	should_record_the_hex_value() {
		assertThat(precipitation.hex(), is(HEX_OF_06));
	}
	
	@Test public void 
	should_convert_hex_to_decimal() {
		assertThat(precipitation.decimal(), is(0.6));
	}
	
	@Test public void 
	should_convert_another_hex_to_decimal() {
		precipitation = new Precipitation(HEX_OF_24);
		
		assertThat(precipitation.decimal(), is(2.4));
	}
	
	@Test public void 
	should_output_value_for_printing() {
		assertThat(precipitation.value(), is("0.6"));
	}
}
