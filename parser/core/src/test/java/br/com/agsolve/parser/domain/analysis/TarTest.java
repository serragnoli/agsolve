package br.com.agsolve.parser.domain.analysis;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("boxing")
public class TarTest {
	
	private static final String HEX_OF_AVG_212_MIN_210_MAX_215 = "D400D200D700";
	private static final String HEX_AVG = "D400";
	private static final String HEX_MIN = "D200";
	private static final String HEX_MAX = "D700";
	private static final String HEX_OF_AVG_209_MIN_208_MAX_210 = "D100D000D200";
	
	private Tar tar;
	
	@Before public void
	setup() {
		tar = new Tar(HEX_OF_AVG_212_MIN_210_MAX_215);
	}
	
	@Test public void 
	should_display_average_header() {
		String header = tar.average().header();
		
		assertThat(header, is(notNullValue(String.class)));
	}

	@Test public void 
	should_display_min_header() {
		String header = tar.min().header();
		
		assertThat(header, is(notNullValue(String.class)));
	}

	@Test public void 
	should_display_max_header() {
		String header = tar.max().header();
		
		assertThat(header, is(notNullValue(String.class)));
	}
	
	@Test public void 
	should_record_average() {
		assertThat(tar.average(), is(notNullValue()));
	}
	
	@Test public void 
	should_record_min() {
		assertThat(tar.min(), is(notNullValue()));
	}

	@Test public void 
	should_record_max() {
		assertThat(tar.max(), is(notNullValue()));
	}
	
	@Test public void 
	should_record_average_in_hex() {
		assertThat(tar.average().hex(), is(HEX_AVG));
	}
	
	@Test public void 
	should_record_min_in_hex() {
		assertThat(tar.min().hex(), is(HEX_MIN));
	}
	
	@Test public void 
	should_record_max_in_hex() {
		assertThat(tar.max().hex(), is(HEX_MAX));
	}
	
	@Test public void 
	should_convert_average_from_hex_to_decimal() {
		assertThat(tar.average().decimal(), is(21.2));
	}
	
	@Test public void 
	should_convert_min_from_hex_to_decimal() {
		assertThat(tar.min().decimal(), is(21.0));
	}
	
	@Test public void 
	should_convert_max_from_hex_to_decimal() {
		assertThat(tar.max().decimal(), is(21.5));
	}
	
	@Test public void 
	should_convert_another_average_from_hex_to_decimal() {
		tar = new Tar(HEX_OF_AVG_209_MIN_208_MAX_210);
		
		assertThat(tar.average().decimal(), is(20.9));
	}
	
	@Test public void 
	should_convert_another_min_from_hex_to_decimal() {
		tar = new Tar(HEX_OF_AVG_209_MIN_208_MAX_210);
		
		assertThat(tar.min().decimal(), is(20.8));
	}
	
	@Test public void 
	should_convert_another_max_from_hex_to_decimal() {
		tar = new Tar(HEX_OF_AVG_209_MIN_208_MAX_210);
		
		assertThat(tar.max().decimal(), is(21.0));
	}
	
	@Test public void 
	should_output_average_value_for_printing() {
		assertThat(tar.average().value(), is("21.2"));
	}
	
	@Test public void 
	should_output_min_value_for_printing() {
		assertThat(tar.min().value(), is("21.0"));
	}
	
	@Test public void 
	should_output_max_value_for_printing() {
		assertThat(tar.max().value(), is("21.5"));
	}
}
