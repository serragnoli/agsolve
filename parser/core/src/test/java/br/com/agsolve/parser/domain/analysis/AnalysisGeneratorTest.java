package br.com.agsolve.parser.domain.analysis;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.AnalysisGenerator;
import br.com.agsolve.parser.domain.analysis.handlers.Handler;
import br.com.agsolve.parser.domain.analysis.handlers.HandlersFactory;
import br.com.agsolve.parser.domain.file.TransposedLines;

public class AnalysisGeneratorTest {
	
	private HandlersFactory factory;
	private AnalysisGenerator generator;
	private TransposedLines transposedLines;
	private Handler handler;

	@Before public void
	setup() {
		factory = mock(HandlersFactory.class);
		transposedLines = mock(TransposedLines.class);
		handler = mock(Handler.class);
		
		generator = new AnalysisGenerator(factory);
	}
	
	@Test public void 
	should_invoke_factory_to_create_report_generation_handlers() {
		when(factory.produceHandlers()).thenReturn(handler);
		
		generator.generate(transposedLines);
		
		verify(factory).produceHandlers();
	}
	
	@Test public void 
	should_invoke_handlers() {
		when(factory.produceHandlers()).thenReturn(handler);
		
		generator.generate(transposedLines);
		
		verify(handler).handle(any(Analysis.class));
	}
	
	@Test public void 
	should_have_toString_overridden() {
		assertThat(generator.toString(), not(containsString("@")));
	}
}
