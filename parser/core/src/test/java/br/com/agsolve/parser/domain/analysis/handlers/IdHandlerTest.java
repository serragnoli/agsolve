package br.com.agsolve.parser.domain.analysis.handlers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;

import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.handlers.IdHandler;

public class IdHandlerTest {

	private IdHandler idHandler = new IdHandler();
	private Analysis analysis = mock(Analysis.class);
	
	@Test public void 
	should_invoke_report_to_extract_id() {
		idHandler.handle(analysis);
		
		verify(analysis).parseId();
	}
}
