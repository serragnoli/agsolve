package br.com.agsolve.parser.domain.analysis;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("boxing")
public class UarTest {

	private static final String HEX_OF_AVG_925_MIN_923_MAX_927 = "9D039B039F03";
	private static final String HEX_AVG = "9D03";
	private static final String HEX_MIN = "9B03";
	private static final String HEX_MAX = "9F03";
	private static final String HEX_OF_AVG_903_MIN_897_MAX_909 = "870381038D03";
	private Uar uar;
	
	@Before public void
	setup() {
		uar = new Uar(HEX_OF_AVG_925_MIN_923_MAX_927);
	}
	
	@Test public void 
	should_display_average_header() {
		String header = uar.average().header();
		
		assertThat(header, is(notNullValue(String.class)));
	}

	@Test public void 
	should_display_min_header() {
		String header = uar.min().header();
		
		assertThat(header, is(notNullValue(String.class)));
	}

	@Test public void 
	should_display_max_header() {
		String header = uar.max().header();
		
		assertThat(header, is(notNullValue(String.class)));
	}
	
	@Test public void 
	should_record_average() {
		assertThat(uar.average(), is(notNullValue()));
	}
	
	@Test public void 
	should_record_min() {
		assertThat(uar.min(), is(notNullValue()));
	}
	
	@Test public void 
	should_record_max() {
		assertThat(uar.max(), is(notNullValue()));
	}
	
	@Test public void 
	should_record_average_in_hex() {
		assertThat(uar.average().hex(), is(HEX_AVG));
	}
	
	@Test public void 
	should_record_min_in_hex() {
		assertThat(uar.min().hex(), is(HEX_MIN));
	}
	
	@Test public void 
	should_record_max_in_hex() {
		assertThat(uar.max().hex(), is(HEX_MAX));
	}
	
	@Test public void 
	should_convert_average_from_hex_to_decimal() {
		assertThat(uar.average().decimal(), is(92.5));
	}
	
	@Test public void 
	should_convert_min_from_hex_to_decimal() {
		assertThat(uar.min().decimal(), is(92.3));
	}
	
	@Test public void 
	should_convert_max_from_hex_to_decimal() {
		assertThat(uar.max().decimal(), is(92.7));
	}
	
	@Test public void 
	should_convert_another_average_from_hex_to_decimal() {
		uar = new Uar(HEX_OF_AVG_903_MIN_897_MAX_909);
		
		assertThat(uar.average().decimal(), is(90.3));
	}
	
	@Test public void 
	should_convert_another_min_from_hex_to_decimal() {
		uar = new Uar(HEX_OF_AVG_903_MIN_897_MAX_909);
		
		assertThat(uar.min().decimal(), is(89.7));
	}
	
	@Test public void 
	should_convert_another_max_from_hex_to_decimal() {
		uar = new Uar(HEX_OF_AVG_903_MIN_897_MAX_909);
		
		assertThat(uar.max().decimal(), is(90.9));
	}
	
	@Test public void 
	should_output_average_value_for_printing() {
		assertThat(uar.average().value(), is("92.5"));
	}
	
	@Test public void 
	should_output_min_value_for_printing() {
		assertThat(uar.min().value(), is("92.3"));
	}
	
	@Test public void 
	should_output_max_value_for_printing() {
		assertThat(uar.max().value(), is("92.7"));
	}
}
