package br.com.agsolve.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.AnalysisGenerator;
import br.com.agsolve.parser.domain.analysis.handlers.HandlersFactory;
import br.com.agsolve.parser.domain.file.TransposedLine;
import br.com.agsolve.parser.domain.file.TransposedLines;

import com.google.common.base.Strings;

public class ParametersBuilder {

	private static final List<String> FILE_HEADER = new ArrayList<>();
	private static final List<String> VALID_LINES = new ArrayList<>();
	private static final List<String> FILE_FOOTER = new ArrayList<>();
	private static final List<String> GIBBERISH_LINES = new ArrayList<>();
	private static final int DEFAULT_LINE_BREAK = 52;
	private static final List<String> TRANSPOSED_LINES = new ArrayList<>();
	private static final String TAB = "\t";
	private static final String REPORTING_HEADER;
	private static final List<String> REPORTING_LINES = new ArrayList<>();
	private static final String CARRIAGE_RETURN = System.getProperty("line.separator");

	static {
		FILE_HEADER.add("2014/03/07 14:48:02  www.agsolve.com.br   Fone: (19) 3825.1991   AgLogger 1 MB V_0160, SN_6822, Reg_ 8333/40960_linhas");
		FILE_HEADER.add("**EMA completa V.3a Jun13** ID:1  Scan(s):10  Log(m):10  Bat(V):10.1  T_Int(C):30");
		FILE_HEADER.add("(L)og    (V)iew    (Z)oom    (O)ffload   (N)ewRun    (C)lock    (P)arams");
		FILE_HEADER.add(")>o");
		FILE_HEADER.add("Data-Hora	Bat	tInt	ID	Tar_md	Tmin	Tmax	Uar_md	Umin	Umax	PAR	Prec");
		FILE_HEADER.add("20FF/FF/FF FF:FF:FF	25.5	77.5	-1	-0.1	-0.1	-0.1	-0.1	-0.1	-0.1	-1	-0.1");
		FILE_HEADER.add("20FF/FF/FF FF:FF:FF	25.5	77.5	-1	-0.1	-0.1	-0.1	-0.1	-0.1	-0.1	-1	-0.1");
		FILE_HEADER.add("20FF/FF/FF FF:FF:FF	25.5	77.5	-1	-0.1	-0.1	-0.1	-0.1	-0.1	-0.1	-1	-0.1");
		FILE_HEADER.add("20FF/FF/FF FF:FF:FF	25.5	77.5	-1	-0.1	-0.1	-0.1	-0.1	-0.1	-0.1	-1	-0.1");
		FILE_HEADER.add("20FF/FF/FF FF:FF:FF	25.5	77.5	-1	-0.1	-0.1	-0.1");
		FILE_HEADER.add("2014/03/07 14:50:17  www.agsolve.com.br   Fone: (19) 3825.1991   AgLogger 1 MB V_0160, SN_6822, Reg_ 8333/40960_linhas");
		FILE_HEADER.add("**EMA completa V.3a Jun13** ID:1  Scan(s):10  Log(m):10  Bat(V):10.0  T_Int(C):32");
		FILE_HEADER.add("(L)og    (V)iew    (Z)oom    (O)ffload   (N)ewRun    (C)lock    (P)arams");
		FILE_HEADER.add(">a");
		
		FILE_FOOTER.add("----0091:FF");
		FILE_FOOTER.add("----0092:FF:");
		FILE_FOOTER.add("----0093:FF:");
		FILE_FOOTER.add("----0094:FF:");
		FILE_FOOTER.add("----0095:FF:");
		FILE_FOOTER.add("----0096:FF:");
		FILE_FOOTER.add("----0097:FF:");
		FILE_FOOTER.add("----0098:FF:");
		FILE_FOOTER.add("----0099:FF:");
		FILE_FOOTER.add("----009A:FF:");
		FILE_FOOTER.add("----009B:FF:");

		VALID_LINES.add("----0000:1402151210002F910100E100E100E2009F039E03A1031D0000001402151220002F910100E200E200E4009D039B039F03210000001402151230002F920100E500E500E6009C039B039F032400000014021512400043930100E800E600EB009C0399039F033100000014021512500046930100EC00EA00EF00980395039C034500000014021513000046950100F100EF00F5009303910397035E00000014021513100044960100F700F400FE008F038D0392037A00000014021513200045980100FF00FE000101870381038D0392000000140215133000459901000101FF00030181037E038503AF00000014021513400047990100050103010A017B037703800377000000FFFFFFFF");
		VALID_LINES.add("----0001:140215135000469A01000C0109010E0172036C0378039D000000140215140000459B01000E010D010F016A0367036E0332010000140215141000459D010011010F01140164036003680373000000140215142000489D01001401130116015E035903620347000000140215143000479D0100140113011701590357035C032A000000140215144000449E0100190117011C0151034C03570320000000140215145000479E01001701150119014C034A034F036C000000140215150000459E01001501130119014A0347034C033A000000140215151000439D0100170112011A01480346034B031E000000140215152000429C01000D010A011201480346034C0315000000FFFFFFFF");
		VALID_LINES.add("----0002:140215153000429B01000A0108010B014C0349034F0334000000140215154000429A0100090106010C01480346034D031100000014021515500041990100030102010701490348034B0319000000140215160000419801000001000102014A0349034C031C00000014021516100041980100FF00FE0001014D034A0350031700000014021516200041970100FD00FD00FE004F034E0350031500000014021516300041970100FB00FB00FD0051034F0354031400000014021516400041960100FB00FA00FC005103500354031200000014021516500040960100F800F700FA005003500351030E00000014021517000040950100F700F600F8005103500353030A000000FFFFFFFF");
		VALID_LINES.add("----0003:14021517100040950100F500F500F7005303520355030900000014021517200040950100F400F400F5005403540355030700000014021517300040940100F300F300F4005503540357030500000014021517400040940100F200EF00F300580355035C030300000014021517500040920100E700E200EF0051034F0355030500000014021518000040900100DD00DB00E1004F034E03500305000000140215181000408F0100DA00DA00DB004D034B03510303000000140215182000408F0100D700D600DA004B034B034E0302000000140215183000408E0100D500D500D6004E034C03510301000000140215184000408E0100D400D400D50053035103550301000000FFFFFFFF");
		VALID_LINES.add("----0004:1402151850003E8E0100D400D400D500570355035A03010000001402151900003E8E0100D500D500D500590357035A03010000001402151910003E8E0100D500D500D600560355035803010000001402151920003E8E0100D400D200D700550354035803010000001402151930003D8D0100D100D000D2005A0358035D03010000001402151940003E8D0100CF00CF00D00060035C036403010000001402151950003E8D0100CE00CE00CF00660363036803010000001402152000003E8C0100CD00CD00CE006A0368036E03010000001402152010003E8C0100CC00CB00CD0070036D037403010000001402152020003D8C0100CB00CA00CC0075037303770301000000FFFFFFFF");
		VALID_LINES.add("----0005:1402152030003D8C0100C800C800CA00790377037D03010000001402152040003D8B0100C700C700C8007E037C038003010000001402152050003D8B0100C700C700C80081037F038203010000001402152100003D8B0100C800C800C800810381038303010000001402152110003D8B0100C800C800CA00810381038303010000001402152120003D8C0100CA00CA00CA00810381038403010000001402152130003D8B0100C900C700CA00850383038803010006001402152140003D8A0100C400C300C7008B0387039103010006001402152150003D8A0100C100C100C300920390039603010002001402152200003D8A0100C100C100C20097039503990301000000FFFFFFFF");
		VALID_LINES.add("----0006:1402152210003D890100C000C000C1009A0399039D03010002001402152220003D890100C000C000C0009D039C039F03010000001402152230003D890100BF00BF00C0009F039F03A103010000001402152240003D890100BE00BE00BF00A103A103A303010000001402152250003D890100BE00BE00BF00A403A303A503010002001402152300003D890100BE00BE00BE00A503A403A703010000001402152310003D890100BE00BE00BE00A703A603A803010000001402152320003D890100BE00BE00BE00A903A803AA03010000001402152330003C890100BE00BE00BF00AA03AA03AC03010000001402152340003C890100BE00BE00BF00AC03AB03AD0301000000FFFFFFFF");
		VALID_LINES.add("----0007:1402152350003C890100BE00BE00BF00AD03AC03AF03010000001402160000003C890100BD00BD00BE00AE03AE03AF03010000001402160010003C890100BD00BD00BD00AF03AE03B103010000001402160020003C890100BD00BD00BD00B003B003B203010002001402160030003C890100BD00BD00BE00B203B003B303010000001402160040003C890100BE00BE00BE00B303B303B403010000001402160050003C890100BE00BE00BE00B403B403B503010000001402160100003B890100BD00BD00BE00B503B503B703010000001402160110003B890100BD00BD00BE00B603B603B803010000001402160120003B890100BD00BD00BD00B703B703B80301000000FFFFFFFF");
		VALID_LINES.add("----0008:1402160130003B890100BC00BC00BD00B803B803B903010000001402160140003B890100BC00BC00BD00B903B803BA03010000001402160150003B890100BD00BD00BD00BA03B903BB03010000001402160200003B890100BC00BC00BD00BB03BA03BC03010000001402160210003B890100BC00BC00BD00BC03BC03BD030100000014021602200040890100BD00BD00BD00BC03BC03BD03010000001402160230003B890100BC00BC00BD00BD03BD03BF03010000001402160240003B890100BC00BC00BC00BE03BE03C003010000001402160250003B890100BC00BC00BC00BF03BF03C103010000001402160300003A890100BC00BC00BC00C103C003C20301000000FFFFFFFF");
		VALID_LINES.add("----0009:1402160310003A890100BC00BC00BC00C103C103C203010000001402160320003B890100BC00BC00BC00C203C103C303010000001402160330003B890100BB00BB00BC00C303C303C403010000001402160340003B880100BB00BB00BC00C403C403C503010000001402160350003B880100BB00BB00BC00C503C503C603010000001402160400003B880100BB00BB00BB00C503C503C603010000001402160410003B880100BB00BB00BB00C603C603C803010000001402160420003B880100BB00BB00BB00C703C703C803010000001402160430003B880100BA00BA00BB00C703C703C903010000001402160440003B880100BA00BA00BB00C803C703C90301000000FFFFFFFF");
		VALID_LINES.add("----000A:1402160450003B880100BA00BA00BA00C803C803CA03010000001402160500003B880100BA00BA00BA00C903C903CB03010000001402160510003B880100BA00BA00BA00C903C903CB03010000001402160520003B880100BA00BA00BA00CA03CA03CB03010000001402160530003B880100BA00BA00BA00CA03CA03CC03010000001402160540003B880100BA00BA00BA00CB03CA03CC03010000001402160550003A880100B900B900BA00CB03CB03CD03010000001402160600003A880100B900B900BA00CC03CB03CD03010000001402160610003A880100B900B900BA00CC03CC03CE03010000001402160620003A880100B900B900BA00CD03CC03CE0301000000FFFFFFFF");
		VALID_LINES.add("----000B:1402160630003A880100B900B900BA00CD03CD03CE03010000001402160640003A880100B900B900BA00CE03CD03CF03010000001402160650003A880100BA00BA00BA00CE03CD03D003010000001402160700003A880100B900B900BA00CE03CD03CF03020000001402160710003A880100BA00BA00BB00CF03CF03D003040000001402160720003A880100BA00BA00BB00CF03CF03D003050000001402160730003A890100BB00BB00BC00D003D003D103060000001402160740003A890100BB00BB00BD00D003D003D103080000001402160750003A890100BC00BC00BE00D103D003D2030A0000001402160800003A890100BD00BD00BF00D103D003D2030C000000FFFFFFFF");
		VALID_LINES.add("----000C:1402160810003A8A0100BF00BE00C100D103D103D303120000001402160820003A8A0100C100C100C200D103D003D203170000001402160830003A8B0100C300C200C500D003CF03D3031A0000001402160840003A8B0100C500C400C700D003CF03D2031D0000001402160850003A8B0100C700C700C800CF03CE03D2031B0000001402160900003A8C0100C700C700C900CE03CD03CF03150000001402160910003A8B0100C700C600C800CD03CC03CF03130000001402160920003A8B0100C600C600C800CE03CD03D003130000001402160930003A8B0100C600C600C700CD03CC03D003160000001402160940003A8B0100C600C600C800CE03CD03CF0318000000FFFFFFFF");
		VALID_LINES.add("----000D:140216095000398B0100C700C600C800CE03CD03D0031A000000140216100000398C0100C800C800C900CE03CD03CF031B000000140216101000398C0100C800C800CA00CE03CE03D0031A000000140216102000398C0100C900C900CA00CE03CE03D003180000001402161030003A8C0100CA00C900CB00CF03CE03D103150000001402161040003A8C0100CA00CA00CB00CF03CE03D003140000001402161050003A8C0100CA00CA00CB00CF03CE03D10315000000140216110000398C0100CB00CB00CC00CF03CE03D10319000000140216111000398C0100CC00CC00CE00D003CE03D2031B000000140216112000398D0100CD00CD00CF00D003CF03D1031C000000FFFFFFFF");
		VALID_LINES.add("----000E:140216113000398D0100CF00CE00D100D003CE03D1031E000000140216114000398D0100D000D000D100CF03CE03D0031F000000140216115000388E0100D000D000D200CE03CD03D1031E000000140216120000398E0100D000D000D200CD03CC03CF031B000000140216121000388E0100D000D000D100CD03CC03CF031E000000140216122000388E0100D000D000D100CF03CD03D1031E000000140216123000388E0100D100D000D200CF03CE03D1031E000000140216124000388E0100D200D100D300CF03CE03D2031F000000140216125000388E0100D300D200D500CF03CE03D1031F000000140216130000388E0100D400D400D500CE03CD03D0031E000000FFFFFFFF");
		VALID_LINES.add("----000F:140216131000388F0100D400D400D600CD03CC03D00325000000140216132000388F0100D600D600D800CD03CC03CF032B000000140216133000398F0100D800D700D900CC03C903CF032F00000014021613400039900100DA00D800DD00CA03C903CC034800000014021613500038910100DF00DC00E300C803C503CC036D00000014021614000038930100E700E300EA00C403C003C8039F00000014021614100038940100EA00E900EC00BD03BA03C2034F00000014021614200038940100EB00EA00ED00B903B603BC034900020014021614300038940100EE00EC00F000B403B003B9034C00000014021614400038950100F100EF00F500AE03AA03B2032A000000FFFFFFFF");
		VALID_LINES.add("----0010:14021614500038970100F700F500FA00A703A103AC034C00000014021615000038960100F200EE00F600A203A003A5032600000014021615100040940100EA00E700EE00A203A103A5031C0000001402161520003F930100E600E500E800A503A303A8032400000014021615300041930100E700E600EA00A503A303A8033200000014021615400041930100EA00EA00EB009F039C03A5032A0000001402161550003C930100EB00EA00ED009E039C03A103250000001402161600003D930100E900E800EA00A203A103A5031D0000001402161610003D930100E700E700E900A503A403A8031F0000001402161620003D930100E800E700EA00A503A303A80320000000FFFFFFFF");
		VALID_LINES.add("----0090:1402251430002AA501001F011B012301C202BC02C802230000001402251440002AA701001B011B011C01C302C102C502230000001402251450002AA701001D011C011F01BF02BC02C5022A0000001402251500002AA8010020011F012201BE02BC02C402220000001402251510002AAA0100210120012401BF02BC02C3021A000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");

		GIBBERISH_LINES.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis turpis quis semper malesuada.	Pellentesque bibendum velit nec tortor pharetra dictum.	Phasellus pharetra urna et nisi iaculis, sit amet sodales leo fermentum. Nunc nec est in tortor bibendum vestibulum quis id est.	Integer eget turpis vel tellus interdum interdum vel sed massa.	Sed ultrices lorem quis nisl laoreet, et placerat felis interdum. Etiam non velit gravida, ultrices leo vel, gravida purus.");
		GIBBERISH_LINES.add("Duis vitae sapien id tellus sodales mollis.	Proin accumsan lectus in diam auctor, sed accumsan dui pretium.	Quisque faucibus massa at egestas dignissim.Ut et velit nec sapien auctor faucibus at eget augue. Ut ut augue et lorem convallis aliquam.	Duis dapibus est ac orci iaculis tincidunt.");
		GIBBERISH_LINES.add("Quisque vel leo vitae erat vulputate viverra quis eget risus.Ut nec leo in nisl tristique ultricies eget et libero.	Phasellus et ante sit amet nisl commodo sodales quis eget est.Phasellus pretium est id magna congue, quis malesuada urna imperdiet.	Nullam consectetur velit sit amet felis auctor, at lobortis velit auctor. Cras at turpis at turpis malesuada dapibus in in leo.");
		GIBBERISH_LINES.add("Sed adipiscing leo quis risus semper, ac ornare dui vehicula. Praesent sit amet velit pharetra, lacinia nibh ut, adipiscing nibh. Nulla elementum leo non erat consectetur blandit. Nam pharetra nulla id ipsum sodales commodo. In convallis sem a metus congue mollis.	Cras vel elit eget massa sagittis tincidunt et non purus.");
		GIBBERISH_LINES.add("Vestibulum egestas mauris vitae nisl fringilla, eget varius massa venenatis. Vestibulum hendrerit libero vitae leo tincidunt, eget ultrices est euismod. Aenean non arcu varius nibh aliquet scelerisque ut quis mauris. Suspendisse id libero mollis, ultricies nunc sit amet, sodales metus. Integer id odio vitae tellus vestibulum mollis.	Aenean blandit nunc dignissim libero mattis, a imperdiet leo blandit.");
		GIBBERISH_LINES.add("Donec porta dui vel orci pulvinar, quis consectetur lectus porta. Fusce tincidunt nisl vel felis rhoncus, at suscipit justo aliquet. Morbi vehicula neque ac accumsan venenatis. Etiam a leo et odio tristique eleifend et sit amet augue. Pellentesque mattis mi sit amet elit iaculis commodo. Vivamus sit amet mi sed augue aliquam lobortis in eu leo.");
		GIBBERISH_LINES.add("Donec congue mi non purus lobortis commodo. Sed adipiscing est vel orci gravida feugiat. Etiam vel elit quis enim consequat suscipit id sit amet odio. Nunc lobortis dui nec gravida pellentesque. Fusce ornare velit ut odio tincidunt gravida. Nulla sed dolor iaculis, varius ligula quis, volutpat quam. Fusce quis metus iaculis, aliquam sem vel, bibendum nisl. Cras ac risus volutpat, consectetur sem at, venenatis purus.	Phasellus volutpat metus ut orci ornare, ut tincidunt nunc fermentum. Nulla id libero posuere, feugiat mauris eget, facilisis lectus.");
		GIBBERISH_LINES.add("Curabitur eu ligula quis turpis dignissim volutpat id at elit. Aliquam et augue eu arcu aliquam lobortis vitae vitae magna.	Proin sed lacus ut nisl elementum sodales. Donec quis metus ornare, euismod lorem id, malesuada magna. Nulla feugiat urna eget lacus lobortis convallis. Duis rhoncus mi ut tellus facilisis semper. Donec rhoncus eros vitae enim convallis, vel tempus libero gravida. Phasellus iaculis nisl ac purus volutpat, in dictum leo condimentum. Morbi tincidunt eros id lacus varius faucibus.");
		GIBBERISH_LINES.add("Donec sit amet nibh condimentum, sagittis massa at, ultricies erat. Fusce eget purus semper, consequat nisl in, dictum nibh. Vestibulum dapibus lectus ut felis vehicula, gravida convallis quam ultricies. Donec interdum justo in quam tincidunt varius. Pellentesque ac quam et nisl porttitor tincidunt dignissim sed nisi. Etiam ultricies nisi eu dui venenatis, eu venenatis turpis ultrices. Pellentesque commodo massa ac lorem eleifend euismod. Proin venenatis ante in sollicitudin commodo. Maecenas ullamcorper arcu eu nulla dictum pulvinar non vel nisi.");

		TRANSPOSED_LINES.add("1402151210002F910100E100E100E2009F039E03A1031D000000");
		TRANSPOSED_LINES.add("1402151220002F910100E200E200E4009D039B039F0321000000");
		TRANSPOSED_LINES.add("1402151230002F920100E500E500E6009C039B039F0324000000");
		TRANSPOSED_LINES.add("14021512500046930100EC00EA00EF00980395039C0345000000");

		REPORTING_HEADER = new StringBuilder("Date/Time").append(TAB)
															.append("Battery").append(TAB)
															.append("Tint").append(TAB)
															.append("Id").append(TAB)
															.append("Tar avg.").append(TAB)
															.append("Tar min.").append(TAB)
															.append("Tar max.").append(TAB)
															.append("Uar avg.").append(TAB)
															.append("Uar min.").append(TAB)
															.append("Uar max.").append(TAB)
															.append("RadPar").append(TAB)
															.append("Precipitation").append(CARRIAGE_RETURN)
															.toString();
		REPORTING_LINES.add(new StringBuilder("15-02-2014 12:10:00").append(TAB)
															.append("4.7").append(TAB)
															.append("22.5").append(TAB)
															.append("1").append(TAB)
															.append("22.5").append(TAB)
															.append("22.5").append(TAB)
															.append("22.6").append(TAB)
															.append("92.7").append(TAB)
															.append("92.6").append(TAB)
															.append("92.9").append(TAB)
															.append("2.9").append(TAB)
															.append("0.0").append(CARRIAGE_RETURN)
															.toString());
		REPORTING_LINES.add(new StringBuilder("15-02-2014 12:20:00").append(TAB)
															.append("4.7").append(TAB)
															.append("22.5").append(TAB)
															.append("1").append(TAB)
															.append("22.6").append(TAB)
															.append("22.6").append(TAB)
															.append("22.8").append(TAB)
															.append("92.5").append(TAB)
															.append("92.3").append(TAB)
															.append("92.7").append(TAB)
															.append("3.3").append(TAB)
															.append("0.0").append(CARRIAGE_RETURN)
															.toString());
		REPORTING_LINES.add(new StringBuilder("15-02-2014 12:30:00").append(TAB)
															.append("4.7").append(TAB)
															.append("23").append(TAB)
															.append("1").append(TAB)
															.append("22.9").append(TAB)
															.append("22.9").append(TAB)
															.append("23").append(TAB)
															.append("92.4").append(TAB)
															.append("92.3").append(TAB)
															.append("92.7").append(TAB)
															.append("3.6").append(TAB)
															.append("0.0").append(CARRIAGE_RETURN)
															.toString());
		REPORTING_LINES.add(new StringBuilder("15-02-2014 12:50:00").append(TAB)
															.append("7").append(TAB)
															.append("23.5").append(TAB)
															.append("1").append(TAB)
															.append("23.6").append(TAB)
															.append("23.4").append(TAB)
															.append("23.9").append(TAB)
															.append("92").append(TAB)
															.append("91.7").append(TAB)
															.append("92.4").append(TAB)
															.append("6.9").append(TAB)
															.append("0.0").append(CARRIAGE_RETURN)
															.toString());
	}

	private List<String> requestedValidLines = new ArrayList<>();
	private ArrayList<String> requestedGibberishLines = new ArrayList<>();
	private List<String> fileHeader = new ArrayList<>();
	private List<String> fileFooter = new ArrayList<>();
	private String lineBreakPosition;
	private boolean addLinesReadParam = true;
	private boolean addUserOptionsKey = true;

	public static String valid(int lineNumber) {
		String validLine = null;

		try {
			validLine = VALID_LINES.get(lineNumber);
		} catch (IndexOutOfBoundsException ex) {
			throw new IllegalArgumentException("Minimum 0 and Maximum " + (VALID_LINES.size() - 1));
		}

		return validLine;
	}

	public static int defaultLineBreak() {
		return DEFAULT_LINE_BREAK;
	}

	public static String transposedLine(int index) {
		String line = null;

		try {
			line = TRANSPOSED_LINES.get(index);
		} catch (IndexOutOfBoundsException ex) {
			throw new IllegalArgumentException("Minimum 0 and Maximum " + (TRANSPOSED_LINES.size() - 1));
		}

		return line;
	}

	public static Analysis analysisWith(int entries) {
		List<TransposedLine> lines = new ArrayList<TransposedLine>();

		for (int i = 0; i < entries; i++) {
			lines.add(new TransposedLine(transposedLine(i)));
		}

		TransposedLines transposedLines = new TransposedLines();
		transposedLines.addAll(lines);

		AnalysisGenerator generator = new AnalysisGenerator(new HandlersFactory());
		return generator.generate(transposedLines);
	}

	public static String reportingHeader() {
		return REPORTING_HEADER;
	}
	
	public static String reportLinesWithHeader(int qty) {
		StringBuilder builder = new StringBuilder();
		builder.append(REPORTING_HEADER);
		
		builder.append(reportLines(qty));
		
		return builder.toString();
	}

	public static String reportLines(int qty) {
		StringBuilder builder = new StringBuilder();

		for(int i = 0; i < qty; i++) {
			builder.append(REPORTING_LINES.get(i));
		}

		return builder.toString();
	}
	

	public static String footers(int qty) {
		StringBuilder builder = new StringBuilder();

		for(int i = 0; i < qty; i++) {
			builder.append(FILE_FOOTER.get(i));
		}

		return builder.toString();
	}


	public ParametersBuilder withValidLines(int numberOfLines) {
		for (int i = 0; i < numberOfLines; i++) {
			this.requestedValidLines.add(VALID_LINES.get(i));
		}
		return this;
	}

	public ParametersBuilder withLineBreakPosition(String on) {
		this.lineBreakPosition = on;
		return this;
	}

	public ParametersBuilder withHeaders() {
		this.fileHeader.addAll(FILE_HEADER);
		return this;
	}
	
	public ParametersBuilder withFooters() {
		this.fileFooter.addAll(FILE_FOOTER);
		return this;
	}

	public ParametersBuilder withGibberishLines(int numberOfLines) {
		for (int i = 0; i < numberOfLines; i++) {
			this.requestedGibberishLines.add(GIBBERISH_LINES.get(i));
		}
		return this;
	}

	public ParametersBuilder noLineBreakPosition() {
		this.lineBreakPosition = null;
		return this;
	}

	public ParametersBuilder noLinesReadParam() {
		addLinesReadParam = false;
		return this;
	}

	public ParametersBuilder withDefaultLineBreakPosition() {
		lineBreakPosition = "52";
		return this;
	}

	public ParametersBuilder noUserOptionsKey() {
		this.addUserOptionsKey = false;
		return this;
	}

	public int linesInFullFile() {
		int total = FILE_HEADER.size() + requestedValidLines.size() + requestedGibberishLines.size();
		return total;
	}

	public Map<Object, Object> build() {
		Map<Object, Object> parameters = new HashMap<>();
		List<String> linesRead = new ArrayList<>();
		Map<String, String> userOptions = new HashMap<>();

		if (!fileHeader.isEmpty()) {
			linesRead.addAll(fileHeader);
		}

		if (!requestedValidLines.isEmpty()) {
			linesRead.addAll(requestedValidLines);
		}

		if (!requestedGibberishLines.isEmpty()) {
			linesRead.addAll(requestedGibberishLines);
		}
		
		if(!fileFooter.isEmpty()) {
			linesRead.addAll(fileFooter);
		}

		if (!Strings.isNullOrEmpty(lineBreakPosition)) {
			userOptions.put("lineBreak", lineBreakPosition);
		}

		if (addUserOptionsKey) {
			parameters.put("userOptions", userOptions);
		}

		if (addLinesReadParam) {
			parameters.put("linesRead", linesRead);
		}
		return parameters;
	}
}
