package br.com.agsolve.parser.domain.report;

import static br.com.agsolve.parser.ParametersBuilder.analysisWith;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.Reportable;

@SuppressWarnings("boxing")
public class ReportBOTest {
	
	private static final Analysis EMPTY_ANALYSIS = analysisWith(0);
	private static final Analysis ONE_LINE_ANALYSIS = analysisWith(1);

	private ReportBO reportBO;
	private ReportableSorter sorter;
	
	@Before public void
	setup() {
		sorter = mock(ReportableSorter.class);
		reportBO = new ReportBO(sorter);
	}
	
	@Test public void 
	should_create_empty_report_when_analysis_is_empty() {
		Report report = reportBO.prepare(EMPTY_ANALYSIS);
		
		assertThat(report.isEmpty(), is(true));
	}
	
	@Test public void 
	should_create_a_report() {
		Report report = reportBO.prepare(ONE_LINE_ANALYSIS);
		
		assertThat(report, is(notNullValue()));
	}
	
	@Test public void 
	should_sort_reportables() {
		reportBO.prepare(ONE_LINE_ANALYSIS);
		
		verify(sorter, atLeastOnce()).sort(anyListOf(Reportable.class));
	}
}
