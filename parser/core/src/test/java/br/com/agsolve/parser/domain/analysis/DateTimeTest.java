package br.com.agsolve.parser.domain.analysis;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("boxing")
public class DateTimeTest {

	private static final String ORIGINAL_VALUE = "140215121000";
	private static final String ANOTHER_ORIGINAL_VALUE = "140205040109";
	
	private DateTime dateTime;

	@Before public void
	setup(){
		dateTime = new DateTime(ORIGINAL_VALUE);
	}
	
	@Test public void 
	should_display_header() {
		String header = dateTime.header();
		
		assertThat(header, is(notNullValue(String.class)));
	}
	
	@Test public void 
	should_record_original_value() {
		assertThat(dateTime.original(), is(ORIGINAL_VALUE));
	}
	
	@Test public void 
	should_convert_year_to_text() {
		assertThat(dateTime.year().text(), is("2014"));
	}
	
	@Test public void 
	should_convert_year_to_decimal() {
		assertThat(dateTime.year().decimal(), is(2014));
	}
	
	@Test public void 
	should_convert_month_to_text() {
		assertThat(dateTime.month().text(), is("02"));
	}
	
	@Test public void
	should_convert_month_to_decimal() {
		assertThat(dateTime.month().decimal(), is(2));
	}
	
	@Test public void 
	should_convert_day_to_text() {
		assertThat(dateTime.day().text(), is("15"));
	}
	
	@Test public void 
	should_convert_day_to_decimal() {
		assertThat(dateTime.day().decimal(), is(15));
	}
	
	@Test public void 
	should_convert_hour_to_text() {
		assertThat(dateTime.hour().text(), is("12"));
	}
	
	@Test public void 
	should_convert_hour_to_decimal() {
		assertThat(dateTime.hour().decimal(), is(12));
	}
	
	@Test public void 
	should_convert_minute_to_text() {
		assertThat(dateTime.minute().text(), is("10"));
	}
	
	@Test public void 
	should_convert_minute_to_decimal() {
		assertThat(dateTime.minute().decimal(), is(10));
	}
	
	@Test public void 
	should_convert_second_to_text() {
		assertThat(dateTime.second().text(), is("00"));
	}
	
	@Test public void 
	should_convert_second_to_decimal() {
		assertThat(dateTime.second().decimal(), is(0));
	}
	
	@Test public void 
	should_convert_year_to_decimal_for_another_value() {
		dateTime = new DateTime(ANOTHER_ORIGINAL_VALUE);
		
		assertThat(dateTime.year().decimal(), is(2014));
	}
	
	@Test public void 
	should_convert_month_to_decimal_for_another_value() {
		dateTime = new DateTime(ANOTHER_ORIGINAL_VALUE);
		
		assertThat(dateTime.month().decimal(), is(2));
	}
	
	@Test public void 
	should_convert_day_to_decimal_for_another_value() {
		dateTime = new DateTime(ANOTHER_ORIGINAL_VALUE);
		
		assertThat(dateTime.day().decimal(), is(5));
	}
	
	@Test public void 
	should_convert_hour_to_decimal_for_another_value() {
		dateTime = new DateTime(ANOTHER_ORIGINAL_VALUE);
		
		assertThat(dateTime.hour().decimal(), is(4));
	}
	
	@Test public void 
	should_convert_minute_to_decimal_for_another_value() {
		dateTime = new DateTime(ANOTHER_ORIGINAL_VALUE);
		
		assertThat(dateTime.minute().decimal(), is(1));
	}
	
	@Test public void 
	should_convert_second_to_decimal_for_another_value() {
		dateTime = new DateTime(ANOTHER_ORIGINAL_VALUE);
		
		assertThat(dateTime.second().decimal(), is(9));
	}
	
	@Test public void 
	should_output_value_for_printing() {
		assertThat(dateTime.value(), is("15-02-2014 12:10:00"));
	}
}
