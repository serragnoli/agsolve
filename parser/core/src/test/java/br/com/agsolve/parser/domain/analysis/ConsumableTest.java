package br.com.agsolve.parser.domain.analysis;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import br.com.agsolve.parser.ParametersBuilder;
import br.com.agsolve.parser.domain.analysis.Consumable;

public class ConsumableTest {
	
	private static final String COMPLETE_LINE = ParametersBuilder.transposedLine(0);
	private static final String AFTER_DATE_PARSE = "2F910100E100E100E2009F039E03A1031D000000";
	private static final String AFTER_BATTERY_PARSE = "910100E100E100E2009F039E03A1031D000000";
	private static final String AFTER_INTERNAL_TEMPERATURE_PARSE = "0100E100E100E2009F039E03A1031D000000";
	private static final String CONSUMABLE_AFTER_ID_PARSE = "E100E100E2009F039E03A1031D000000";
	private static final String CONSUMABLE_AFTER_TAR_PARSE = "9F039E03A1031D000000";
	private static final String CONSUMABLE_AFTER_UAR_PARSE = "1D000000";
	private static final String CONSUMABLE_AFTER_RAD_PAR_PARSE = "0000";
	private static final String CONSUMABLE_AFTER_PRECIPITATION_PARSE = "";
	
	private Consumable consumable;

	@Test public void 
	should_reduce_consumable_when_date_is_parsed() {
		consumable = new Consumable(COMPLETE_LINE);
		
		consumable.consumeDate();
		
		assertThat(consumable.display(), is(AFTER_DATE_PARSE));
	}
	
	@Test public void 
	should_reduce_consumable_when_battery_is_parsed() {
		consumable = new Consumable(AFTER_DATE_PARSE);
		
		consumable.consumeBattery();
		
		assertThat(consumable.display(), is(AFTER_BATTERY_PARSE));
	}
	
	@Test public void 
	should_reduce_consumable_when_internal_temperature_is_parsed() {
		consumable = new Consumable(AFTER_BATTERY_PARSE);
		
		consumable.consumeInternalTemperature();
		
		assertThat(consumable.display(), is(AFTER_INTERNAL_TEMPERATURE_PARSE));
	}
	
	@Test public void 
	should_reduce_consumable_when_id_is_parsed() {
		consumable = new Consumable(AFTER_INTERNAL_TEMPERATURE_PARSE);
		
		consumable.consumeId();
		
		assertThat(consumable.display(), is(CONSUMABLE_AFTER_ID_PARSE));
	}
	
	@Test public void 
	should_reduce_consumable_when_tar_is_parsed() {
		consumable = new Consumable(CONSUMABLE_AFTER_ID_PARSE);
		
		consumable.consumeTar();
		
		assertThat(consumable.display(), is(CONSUMABLE_AFTER_TAR_PARSE));
	}
	
	@Test public void 
	should_reduce_consumable_when_uar_is_parsed() {
		consumable = new Consumable(CONSUMABLE_AFTER_TAR_PARSE);
		
		consumable.consumeUar();
		
		assertThat(consumable.display(), is(CONSUMABLE_AFTER_UAR_PARSE));
	}
	
	@Test public void 
	should_reduce_consumable_when_rad_par_is_parsed() {
		consumable = new Consumable(CONSUMABLE_AFTER_UAR_PARSE);
		
		consumable.consumeRadPar();
		
		assertThat(consumable.display(), is(CONSUMABLE_AFTER_RAD_PAR_PARSE));
	}

	@Test public void 
	should_reduce_consumable_when_precipitation_is_parsed() {
		consumable = new Consumable(CONSUMABLE_AFTER_RAD_PAR_PARSE);
		
		consumable.consumePrecipitation();
		
		assertThat(consumable.display(), is(CONSUMABLE_AFTER_PRECIPITATION_PARSE));
	}
	
	@Test public void 
	should_have_toString_overridden() {
		consumable = new Consumable(COMPLETE_LINE);
		
		assertThat(consumable.toString(), not(containsString("@")));
	}
}
