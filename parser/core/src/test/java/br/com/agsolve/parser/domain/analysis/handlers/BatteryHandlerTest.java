package br.com.agsolve.parser.domain.analysis.handlers;

import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.agsolve.parser.domain.analysis.Analysis;
import br.com.agsolve.parser.domain.analysis.handlers.BatteryHandler;

public class BatteryHandlerTest {

	private Analysis analysis;
	private BatteryHandler battHandler;
	
	@Before public void
	setup() {
		analysis = Mockito.mock(Analysis.class);
		battHandler = new BatteryHandler();
	}
	
	@Test public void 
	should_invoke_report_to_extract_battery() {
		battHandler.handle(analysis);
		
		verify(analysis).parseBattery();
	}
}
