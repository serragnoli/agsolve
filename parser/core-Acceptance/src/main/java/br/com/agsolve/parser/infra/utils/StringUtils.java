package br.com.agsolve.parser.infra.utils;

public class StringUtils {

	public static String compare(String string1, String string2) {
		if (string1.length() > string2.length()) {
			return iterateStrings(string1, string2);
		}
		
		return iterateStrings(string2, string1);
	}

	private static String iterateStrings(String longest, String shortest) {
		StringBuilder builder1 = new StringBuilder(longest);
		StringBuilder builder2 = new StringBuilder(shortest);
		StringBuilder result = new StringBuilder(longest.length());

		char st1Char = ' ';
		char st2Char = ' ';

		for (int i = 0; i < longest.length(); i++) {
			st1Char = builder1.charAt(i);
			st2Char = i < builder2.length() ? builder2.charAt(i) : ' ';

			if (st1Char == st2Char) {
				result.append(st1Char);
			} else {
				result.append(">>>")
						.append(st1Char)
						.append("<<<")
						.append(longest.substring(i + 1));
				return result.toString();
			}
		}
		return longest;
	}
}
