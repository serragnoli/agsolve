Feature: Parse List<br/>
	As a User
	I want my raw lines of data converted to a tab-separated format
	So that I can open it in an Excel or LibreOffice spreadsheet	

Scenario: Parameter contains headers and 1 line of raw data
	Given the following list of raw lines:
		|20FF/FF/FF FF:FF:FF	25.5	77.5	-1	-0.1	-0.1	-0.1|
		|2014/03/07 14:50:17  www.agsolve.com.br   Fone: (19) 3825.1991   AgLogger 1 MB V_0160, SN_6822, Reg_ 8333/40960_linhas|
		|**EMA completa V.3a Jun13** ID:1  Scan(s):10  Log(m):10  Bat(V):10.0  T_Int(C):32|
		|(L)og    (V)iew    (Z)oom    (O)ffload   (N)ewRun    (C)lock    (P)arams|
		|>a|
		|----0000:1402151210002F910100E100E100E2009F039E03A1031D0000001402151220002F910100E200E200E4009D039B039F03210000001402151230002F920100E500E500E6009C039B039F032400000014021512400043930100E800E600EB009C0399039F033100000014021512500046930100EC00EA00EF00980395039C034500000014021513000046950100F100EF00F5009303910397035E00000014021513100044960100F700F400FE008F038D0392037A00000014021513200045980100FF00FE000101870381038D0392000000140215133000459901000101FF00030181037E038503AF00000014021513400047990100050103010A017B037703800377000000FFFFFFFF|
		|----0091:FF|
		|----00FF:FF|
	When the list is sent to generate the report
	Then the report will contain
		"""
Date/Time	Battery	Tint	Id	Tar avg.	Tar min.	Tar max.	Uar avg.	Uar min.	Uar max.	RadPar	Precipitation
15-02-2014 12:10:00	4.7	22.5	1	22.5	22.5	22.6	92.7	92.6	92.9	2.9	0.0
15-02-2014 12:20:00	4.7	22.5	1	22.6	22.6	22.8	92.5	92.3	92.7	3.3	0.0
15-02-2014 12:30:00	4.7	23.0	1	22.9	22.9	23.0	92.4	92.3	92.7	3.6	0.0
15-02-2014 12:40:00	6.7	23.5	1	23.2	23.0	23.5	92.4	92.1	92.7	4.9	0.0
15-02-2014 12:50:00	7.0	23.5	1	23.6	23.4	23.9	92.0	91.7	92.4	6.9	0.0
15-02-2014 13:00:00	7.0	24.5	1	24.1	23.9	24.5	91.5	91.3	91.9	9.4	0.0
15-02-2014 13:10:00	6.8	25.0	1	24.7	24.4	25.4	91.1	90.9	91.4	12.2	0.0
15-02-2014 13:20:00	6.9	26.0	1	25.5	25.4	25.7	90.3	89.7	90.9	14.6	0.0
15-02-2014 13:30:00	6.9	26.5	1	25.7	25.5	25.9	89.7	89.4	90.1	17.5	0.0
15-02-2014 13:40:00	7.1	26.5	1	26.1	25.9	26.6	89.1	88.7	89.6	11.9	0.0

		"""
		
Scenario: Parameter contains gibberish raw data
	Given the following list of raw lines:
		|<89>PNG^M|
		|^Z|
		|^@^@^@^MIHDR^@^@^@<80>^@^@^@<80>^H^F^@^@^@Ã>aË^@^@^@^YtEXtSoftware^@Adobe ImageReadyqÉe<^@^@^^OIDATxÚì] <9c>SÕ¹ÿîÍ¾M2<99>av``<86>¥Ã¢@^E+V<94>ZµVQ^Q·n¼>[¥µÔ×<8a>^K>kÕºWûóI[´ú\ú¬<8a>^E*R[­ÖZ<85>¾ªÏª¨Ã6ÌÎìÉdßï½ï'¹<99>;^Ypr<93>Ü<90>ar~<9c>_B&¹÷Üóý¿ÿ·<9c><8d>^Q^D^A<8a>eò^V¶Ø^EE^@^TK^Q^@ÅR^D@±^T^AP,E^@^TK^Q^@Å2<99><8a>z¼/0^L3îEì7=»°,âº¨$æ_n<8c><85><9a>ÔB´<82>^U^DÍdèÀ¿>ò3&<97>×³Þ¼ejYØµÆ^Zõ®4r¡ùZ>R+öe<94>U^O^FUú}^^µy·Sg}iðÞoüïx×^[/ÏÃ<8c>û<85>Ï^@@Í^MO<9d>W^WèûÉìrËÒS¿°^L^ZgÔC<99>½^TÊt,¨'^A·üÛ^M·Ã_~u[N^@`ÛøÃô@Ï=õ<9a>èÅ+<96><9f>Â6Í<99>^M<95>^UåÉ|
		|vgçÅ<90>^K^@X¢¾Sm$Þ<8c>D#^T<81>©^Mì^_^X^D<8d>F^C^V³     ^Lz]\;<8e>c^@ 6ªTlÆ><80>-â9^G^]i|
		|^@×X<8d>Æÿ#Åûý~^Z^YdÒF<9d>N^G&<96><9f>fÛðÔ\òÑ¾¬^@ <8f><85>æhI<8c><8a>(<8c>^Q<87>åH^EÃ^P<87>3B;^EãYô^Mð7è<94>^\g^H  gÉseÂ^@Æë<9f>±<98><û^ZK,fðx}É^P^Z^]AdTü^?<98>ôåx      t@`4^Z©òf^M^@^M^_«B­Æ^F§sã^Py^P¬¢=B^P¨±ªU<94>^]Ä:^Q´^]^K|
		|CtþPø<91>H4á^CÈ^G<80><96><8f>ÖZ,^V^H^RÍ^V<9d>é\gNE'Õ`0<82>Ñ^UZ<98>µ     À^KF<89>÷^_<89>D2j^L²^Fe<8e>ðXgEZE»ÊÀÈøCüoY)ì<98><8c>^]^U¦Ä£^W+¤v¤<82>`À¾P^SÇ^W<81>-Wxj.Za,1ÒçB<85>R"m.ö<9d><86>0°>æiÊ^^^@ÀDÃ<91>¨^F<85><98>Ë^FOÄ1<83>#<81>FNQ 1<9b>^¯§àA;¯D^_<88>^L<80> %<8c>S7Þ÷Çåâ0«î^H^D^BP,£Y+^S^P«xÎª%^Ns<92>^U^Uj#Õl^B^@Â8<95>Y^C  Ò^?èÁ4eq^AI¼^O2Ôþxgó:<8c><98><94>L¡<8b>ì<84>,£^R8[Ö&À­6ýÕáp¬©­­+<82>à^Hþ<82>¬<9f>ó<<8b>^Z<9a>ks:¦<8d>2Ìì¸^L0¬±l^_^X^\<8a>Å<88>^G,ð<^TË<88>w)W<88>¬ è0^ZRR<91>ä^y\^@ø^_úÎÀ Êô¼Ó1^TG=<82> A<85><93>®^^)ÄÈ@<4<8c>Ìw{3^E^@<96>ÃúòÛ»^O÷|
	When the list is sent to generate the report
	Then the report will contain
		"""
		No data to be displayed in this report
		
		"""