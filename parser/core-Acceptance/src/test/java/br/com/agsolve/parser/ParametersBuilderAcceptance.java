package br.com.agsolve.parser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParametersBuilderAcceptance {
	private String lineBreakPosition = "52";
	private List<String> rawLines;
	
	public ParametersBuilderAcceptance withLinesRead(List<String> inRawFormat) {
		this.rawLines = inRawFormat;
		return this;
	}

	public ParametersBuilderAcceptance withDefaultLineBreakPosition() {
		return this;
	}

	public Map<Object, Object> build() {		
		Map<String, String> userOptions = new HashMap<>();
		userOptions.put("lineBreak", lineBreakPosition);
		
		Map<Object, Object> parameters = new HashMap<>();
		parameters.put("userOptions", userOptions);		
		parameters.put("linesRead", rawLines);
		
		return parameters;
	}
}
