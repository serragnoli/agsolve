package br.com.agsolve.parser.core.actions;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import br.com.agsolve.parser.ParametersBuilderAcceptance;
import br.com.agsolve.parser.actions.CreateReportFromRawLines;
import br.com.agsolve.parser.domain.report.Report;
import br.com.agsolve.parser.infra.spring.IntegrationTestCoreBeanConfig;
import br.com.agsolve.parser.infra.utils.StringUtils;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ParseListStepDefinitions {

	private CreateReportFromRawLines parser;
	
	private Report report;	
	private Map<Object, Object> params = new HashMap<>();
	private AnnotationConfigApplicationContext context;
	
	private ParametersBuilderAcceptance builder = new ParametersBuilderAcceptance();
	
	@Before public void 
	setup() {
		builder.withDefaultLineBreakPosition();
		
		context = new AnnotationConfigApplicationContext(IntegrationTestCoreBeanConfig.class);
		parser = context.getBean(CreateReportFromRawLines.class);
	}
	
	@Given("^the following list of raw lines:$") public void 
	prepare_map_with_two_raw_lines(List<String> rawLines) {
		params = builder.withLinesRead(rawLines).build();
	}
	
	@When("^the list is sent to generate the report$") public void
	invoke_parser() {
		report = parser.parseRawLines(params);
	}
	
	@Then("^the report will contain") public void
	validate_report(String expectedReport) {
		String result = StringUtils.compare(report.outputAsTabSeparated(), expectedReport);
		assertThat(result, is(expectedReport));
	}
	
	@After
	public void tearDown() {
		context.close();
	}
}
