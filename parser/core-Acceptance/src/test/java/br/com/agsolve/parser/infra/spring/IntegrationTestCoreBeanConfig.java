package br.com.agsolve.parser.infra.spring;

import java.util.Comparator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.agsolve.parser.actions.CreateReportFromRawLines;
import br.com.agsolve.parser.domain.analysis.AnalysisBO;
import br.com.agsolve.parser.domain.analysis.AnalysisGenerator;
import br.com.agsolve.parser.domain.analysis.Reportable;
import br.com.agsolve.parser.domain.analysis.handlers.HandlersFactory;
import br.com.agsolve.parser.domain.file.FileBO;
import br.com.agsolve.parser.domain.report.ReportBO;
import br.com.agsolve.parser.domain.report.ReportableComparator;
import br.com.agsolve.parser.domain.report.ReportableSorter;
import br.com.agsolve.parser.domain.userinput.UserInputBO;

@Configuration
public class IntegrationTestCoreBeanConfig {
	
	@Bean
	public CreateReportFromRawLines createReportFromRawLines() {
		return new CreateReportFromRawLines(userInputBO(), fileBO(), analysisBO(), reportBO());
	}
	
	private UserInputBO userInputBO() {
		return new UserInputBO();
	}

	@Bean
	public FileBO fileBO() {
		return new FileBO();
	}

	@Bean
	public AnalysisBO analysisBO() {
		return new AnalysisBO(analysisGenerator());
	}
	
	@Bean
	public ReportBO reportBO() {
		return new ReportBO(reportableSorter());
	}
	
	private ReportableSorter reportableSorter() {
		return new ReportableSorter(reportableComparator());
	}

	private Comparator<Reportable> reportableComparator() {
		return new ReportableComparator();
	}

	private AnalysisGenerator analysisGenerator() {
		return new AnalysisGenerator(analysisHandlersFactory());
	}

	private HandlersFactory analysisHandlersFactory() {
		return new HandlersFactory();
	}
}
