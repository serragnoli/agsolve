package br.com.agsolve.parser.infra.utils;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class StringUtilsTest {

	@Test public void 
	should_return_where_string_is_different_when_first_is_the_longest() {
		String result = StringUtils.compare("ABCDEFGH", "ABCEDFG");
		
		assertThat(result, is("ABC>>>D<<<EFGH"));
	}
	
	@Test public void 
	should_return_where_string_is_different_when_first_is_the_shortest() {
		String result = StringUtils.compare("ABCEDFG", "ABCDEFGH");
		
		assertThat(result, is("ABC>>>D<<<EFGH"));
	}
}
