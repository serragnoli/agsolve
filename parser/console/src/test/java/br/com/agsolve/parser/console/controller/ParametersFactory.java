package br.com.agsolve.parser.console.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParametersFactory {

	public static String[] inPortuguese() {
		String[] parameters = new String[3];
		
		parameters[0] = "origem=./target/test-classes/twoLines.txt";
		parameters[1] = "destino=./target/test-classes/twoLines-parsed.txt";
		parameters[2] = "quebra=52";
		
		return parameters;
	}

	public static String[] inEnglish() {
		String[] parameters = new String[3];
		
		parameters[0] = "source=./target/test-classes/twoLines.txt";
		parameters[1] = "target=./target/test-classes/twoLines-parsed.txt";
		parameters[2] = "lineBreak=52";
		
		return parameters;
	}

	public static String[] wrongFileLocation() {
		String[] parameters = new String[3];
		
		parameters[0] = "source=./doesntExist.txt";
		parameters[1] = "target=./target/test-classes/twoLines-parsed.txt";
		parameters[2] = "lineBreak=52";
		
		return parameters;
	}

	public static Map<Object, Object> shippingParameters() {
		Map<Object, Object> userOptions = new HashMap<>();
		
		Map<String, String> options = userOptions();		
		List<String> linesRead = linesRead();
		
		userOptions.put("userOptions", options);
		userOptions.put("linesRead", linesRead);
		
		return userOptions;
	}

	public static Map<String, String> userOptions() {
		Map<String, String> options = new HashMap<>();
		options.put("source", "./target/test-classes/twoLines.txt");
		options.put("target", "./target/test-classes/twoLines-userChosenName.txt");
		options.put("lineBreak", "52");

		return options;
	}

	public static List<String> linesRead() {
		List<String> linesRead = new ArrayList<>();
		linesRead.add("Lines 1");
		
		return linesRead;
	}
	

	public static Map<Object, Object> shippingParametersMissingTarget() {
		Map<Object, Object> userOptions = new HashMap<>();
		
		Map<String, String> options = userOptionsMissingTarget();		
		List<String> linesRead = linesRead();
		
		userOptions.put("userOptions", options);
		userOptions.put("linesRead", linesRead);
		
		return userOptions;
	}
	
	public static Map<String, String> userOptionsMissingTarget() {
		Map<String, String> options = new HashMap<>();
		options.put("source", "./target/test-classes/twoLines.txt");
		options.put("lineBreak", "52");

		return options;
	}

}
