package br.com.agsolve.parser.console.view;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.console.controller.ParseController;

public class ConsoleViewerTest {

	private static final String[] ARGS = new String[]{"Test 1"};
 	private ParseController parseController;
	private ConsoleViewer viewer;

	@Before public void 
	setup() {
		parseController = mock(ParseController.class);
	}

	@Test public void 
	should_delegate_to_controller() {
		viewer = new ConsoleViewer(parseController);
		
		viewer.parse(ARGS);
		
		verify(parseController).parse(ARGS);
	}
	
	@Test public void 
	should_be_able_to_create_object() {
		viewer = new ConsoleViewer();
		
		assertThat(viewer, is(notNullValue()));
	}
}
