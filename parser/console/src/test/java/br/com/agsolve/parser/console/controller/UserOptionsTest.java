package br.com.agsolve.parser.console.controller;

import static br.com.agsolve.parser.console.controller.ParametersFactory.linesRead;
import static br.com.agsolve.parser.console.controller.ParametersFactory.shippingParameters;
import static br.com.agsolve.parser.console.controller.ParametersFactory.shippingParametersMissingTarget;
import static br.com.agsolve.parser.console.controller.ParametersFactory.userOptions;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Map;

import org.junit.Test;

@SuppressWarnings("boxing")
public class UserOptionsTest {

	private Map<Object, Object> shippingParameters = shippingParameters();
	private Map<Object, Object> shippingParametersMissingTarget = shippingParametersMissingTarget();
	
	@Test public void 
	should_record_map() {
		UserOptions userOptions = new UserOptions(shippingParameters);
		
		assertThat(userOptions.parameters(), is(notNullValue()));
	}
	
	@Test public void 
	should_return_only_user_options() {
		UserOptions userOptions = new UserOptions(shippingParameters);
		
		assertThat(userOptions.allUserOptions(), is(userOptions()));
	}
	
	@Test public void 
	should_return_lines_that_were_read() {
		UserOptions userOptions = new UserOptions(shippingParameters);
		
		assertThat(userOptions.linesRead(), is(linesRead()));
	}
	
	@Test public void 
	should_retrieve_file_target_location_when_one_is_provided() {
		UserOptions userOptions = new UserOptions(shippingParameters);
		
		assertThat(userOptions.fileTargetLocation(), is("./target/test-classes/twoLines-userChosenName.txt"));
	}
	
	@Test public void 
	should_provide_a_default_file_location_when_one_is_not_provided() {
		UserOptions userOptions = new UserOptions(shippingParametersMissingTarget);
		
		assertThat(userOptions.fileTargetLocation(), is("./target/test-classes/twoLines-PARSED.txt"));
	}
	
	@Test public void 
	should_return_volume_of_lines_read() {
		UserOptions userOptions = new UserOptions(shippingParameters);
		
		assertThat(userOptions.linesReadVolume(), is(1));
	}
}
