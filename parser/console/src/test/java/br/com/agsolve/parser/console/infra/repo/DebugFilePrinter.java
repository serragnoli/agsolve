package br.com.agsolve.parser.console.infra.repo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import br.com.agsolve.parser.console.controller.FilePrinter;

public class DebugFilePrinter implements FilePrinter {

	
	@Override
	public List<String> load(String from) {
		List<String> linesRead = new ArrayList<>();

		try {
			Path path = FileSystems.getDefault()
									.getPath(from);
			linesRead = Files.readAllLines(path, Charset.forName("UTF-8"));
		} catch (IOException e) {
			throw new IllegalArgumentException("File location provided could not be loaded. Please chec the location");
		}
		
		return linesRead;
	}

	@Override
	public void save(String outputText, String into) {
		// TODO Auto-generated method stub
		
	}
}
