package br.com.agsolve.parser.console.view;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

public class RunnerTest {

	private static final String[] ARGS = new String[]{"test1"};
	private ConsoleViewer viewer;
	
	@Before public void
	setup() {
		viewer = mock(ConsoleViewer.class);
		Runner.setViewer(viewer);
	}
	
	@Test public void 
	should_invoke_controller() {
		Runner.main(ARGS);
		
		verify(viewer).parse(ARGS);
	}
}
