package br.com.agsolve.parser.console.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import br.com.agsolve.parser.console.controller.ParametersValidator;
import br.com.agsolve.parser.console.controller.ValidationCollator;

@SuppressWarnings("boxing")
public class ParametersValidatorTest {
	
	private static final String [] EMPTY_PARAMS = new String[]{};
	private static final String [] MISSING_EQUALS_SIGN = new String[]{"keyvalue"};
	private static final String [] NO_KEY_AND_NO_VALUE = new String[]{"="};
	private static final String [] MISSING_KEY = new String[]{"=nokey"};
	private static final String [] MISSING_VALUE = new String[]{"onlykey="};
	private static final String [] ONE_PARAM = new String[]{"key1=value1"};
	private static final String [] TWO_PARAMS = new String[]{"key2=value2", "key3=value3"};
	
	private ParametersValidator validator = new ParametersValidator();
	
	@Test(expected = IllegalArgumentException.class) public void 
	should_throw_exception_when_parameters_are_empty() {
		validator.validate(EMPTY_PARAMS);
	}
	
	@Test(expected = IllegalArgumentException.class) public void 
	should_throw_exception_when_parameters_are_null() {
		validator.validate(null);
	}
	
	@Test public void 
	should_fail_validation_when_malformed_parameter_has_no_equal_sign() {
		ValidationCollator validation = validator.validate(MISSING_EQUALS_SIGN);
		
		assertThat(validation.hasErrors(), is(true));
	}
	
	@Test public void 
	should_fail_validation_when_malformed_parameter_has_only_key() {
		ValidationCollator validation = validator.validate(MISSING_VALUE);
		
		assertThat(validation.hasErrors(), is(true));
	}
	
	@Test public void
	should_fail_validation_when_malformed_parameter_has_only_value() {
		ValidationCollator validation = validator.validate(MISSING_KEY);
		
		assertThat(validation.hasErrors(), is(true));
	}
	
	@Test public void
	should_fail_validation_when_malformed_parameter_has_no_key_and_no_value() {
		ValidationCollator validation = validator.validate(NO_KEY_AND_NO_VALUE);
		
		assertThat(validation.totalErrors(), is(2));
	}
	
	@Test public void
	should_pass_validation_when_parameter_is_well_formed() {
		ValidationCollator validation = validator.validate(ONE_PARAM);
		
		assertThat(validation.hasErrors(), is(false));
	}
	
	@Test public void 
	should_pass_validation_when_double_parameter_is_well_formed() {
		ValidationCollator validation = validator.validate(TWO_PARAMS);
		
		assertThat(validation.hasErrors(), is(false));
	}
}
