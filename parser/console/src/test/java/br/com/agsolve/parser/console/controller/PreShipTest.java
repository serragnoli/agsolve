package br.com.agsolve.parser.console.controller;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import br.com.agsolve.parser.console.infra.repo.DebugFilePrinter;

@SuppressWarnings("boxing")
public class PreShipTest {

	private String[] paramsInPortuguese = ParametersFactory.inPortuguese();
	private String[] paramsInEnglish = ParametersFactory.inEnglish();
	private String[] paramsWithWrongFile = ParametersFactory.wrongFileLocation();
	private FilePrinter fileRepository = new DebugFilePrinter();
	private PreShip preShip = new PreShip(fileRepository);

	@Test public void 
	should_translate_parameter_keys_from_portuguese_to_english() {
		UserOptions readyToShip = preShip.prepare(paramsInPortuguese);
		
		Map<String, String> options = readyToShip.allUserOptions();
		assertThat(options.get("lineBreak"), is(notNullValue()));
	}
	
	@Test public void 
	should_keep_parameter_keys_in_english_when_already_in_english() {
		UserOptions readyToShip = preShip.prepare(paramsInEnglish);
		
		Map<String, String> options = readyToShip.allUserOptions();
		assertThat(options.get("lineBreak"), is(notNullValue()));
	}

	@Test public void 
	should_load_file_when_file_when_file_exists() {
		UserOptions readyToShip = preShip.prepare(paramsInPortuguese);
		
		List<String> linesRead = readyToShip.linesRead();
		
		assertThat(linesRead.size(), is(2));
	}
	
	@Test(expected = IllegalArgumentException.class) public void 
	should_throw_exception_when_file_not_found() {
		preShip.prepare(paramsWithWrongFile);
	}
}
