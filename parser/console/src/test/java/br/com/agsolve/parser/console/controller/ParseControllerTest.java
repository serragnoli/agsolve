package br.com.agsolve.parser.console.controller;

import static br.com.agsolve.parser.console.controller.ParametersFactory.shippingParameters;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import br.com.agsolve.parser.actions.CreateReportFromRawLines;
import br.com.agsolve.parser.domain.report.Report;

public class ParseControllerTest {
	
	private static final String[] PARAMETERS = ParametersFactory.inEnglish();
	
	private CreateReportFromRawLines createReportFromRawLines;
	private ParseController parseController;
	private ParametersValidator validator;
	private ValidationCollator validationWithErrors;
	private ValidationCollator validationNoErrors;
	private PreShip preShip;
	private PostShip postShip;
	private UserOptions userOptions;
	private Report report;
	
	@Before public void
	setup() {
		userOptions = new UserOptions(shippingParameters());
		report = mock(Report.class);
		createReportFromRawLines = mock(CreateReportFromRawLines.class);
		validator = mock(ParametersValidator.class);
		preShip = mock(PreShip.class);
		postShip = mock(PostShip.class);
		parseController = new ParseController(createReportFromRawLines, validator, preShip, postShip);

		validationNoErrors = new ValidationCollator();
		validationWithErrors = new ValidationCollator();
		validationWithErrors.addErrorMessage("ERROR");
	}
	
	@Test public void 
	should_invoke_parameters_validator() {
		when(preShip.prepare(PARAMETERS)).thenReturn(userOptions);
		when(validator.validate(PARAMETERS)).thenReturn(validationNoErrors);
		
		parseController.parse(PARAMETERS);
		
		verify(validator).validate(PARAMETERS);
	}
	
	@Test public void 
	should_throw_exception_when_malformed_parameters() {
		when(validator.validate(PARAMETERS)).thenReturn(validationWithErrors);
		
		try {
			parseController.parse(PARAMETERS);
			fail("Should have thrown an exception");
		} catch(IllegalArgumentException ex) {
			//Success
		}
	}
	
	@Test public void 
	should_delegate_to_parameters_for_shipping_preparation() {
		when(validator.validate(PARAMETERS)).thenReturn(validationNoErrors);
		when(preShip.prepare(PARAMETERS)).thenReturn(userOptions);
		when(createReportFromRawLines.parseRawLines(userOptions.parameters())).thenReturn(report);
		
		parseController.parse(PARAMETERS);
		
		verify(preShip).prepare(PARAMETERS);
	}
	
	@Test public void 
	should_delegate_to_domain_when_no_issues_found() {
		when(preShip.prepare(PARAMETERS)).thenReturn(userOptions);
		when(validator.validate((PARAMETERS))).thenReturn(validationNoErrors);
		
		parseController.parse(PARAMETERS);
		
		verify(createReportFromRawLines).parseRawLines(anyMapOf(Object.class, Object.class));
	}
	
	@Test public void 
	should_delegate_returned_report_to_post_ship() {
		when(preShip.prepare(PARAMETERS)).thenReturn(userOptions);
		when(validator.validate(PARAMETERS)).thenReturn(validationNoErrors);
		
		parseController.parse(PARAMETERS);
		
		verify(postShip).wrapUp(any(Report.class), any(String.class));
	}
	
	@Test public void 
	should_output_stats() {
		when(preShip.prepare(PARAMETERS)).thenReturn(userOptions);
		when(validator.validate(PARAMETERS)).thenReturn(validationNoErrors);
		
		parseController.parse(PARAMETERS);
		
		verify(postShip).prepareAndPrintStats(any(UserOptions.class), any(Report.class), anyLong());
	}
}
