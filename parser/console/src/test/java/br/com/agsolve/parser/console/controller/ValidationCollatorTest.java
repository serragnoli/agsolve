package br.com.agsolve.parser.console.controller;

import static br.com.agsolve.parser.console.controller.ValidationCollator.CARRIAGE_RETURN;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("boxing")
public class ValidationCollatorTest {

	private static final String ERROR = "Something is wrong";
	private ValidationCollator validation;

	@Before public void 
	setup() {
		validation = new ValidationCollator();
	}
	
	@Test public void 
	should_have_errors_when_error_messages_exist() {
		validation.addErrorMessage(ERROR);
		
		assertThat(validation.hasErrors(), is(true));
	}
	
	@Test public void 
	should_not_have_errors_when_none_is_passed() {
		assertThat(validation.hasErrors(), is(false));
	}
	
	@Test public void 
	should_have_two_errors_when_two_errors_collated() {
		validation.addErrorMessage(ERROR);
		validation.addErrorMessage(ERROR);
		
		assertThat(validation.totalErrors(), is(2));
	}
	
	@Test public void 
	should_output_collated_error_messages() {
		validation.addErrorMessage(ERROR);
		validation.addErrorMessage(ERROR);
		
		assertThat(validation.message(), is(ERROR + CARRIAGE_RETURN + ERROR + CARRIAGE_RETURN));
	}
}
