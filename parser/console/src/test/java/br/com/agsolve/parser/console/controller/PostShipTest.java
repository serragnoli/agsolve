package br.com.agsolve.parser.console.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.agsolve.parser.domain.report.Report;

@SuppressWarnings("boxing")
public class PostShipTest {

	private PostShip postShip;
	private FilePrinter fileRepository;
	private StatsPrinter statsPrinter;
	private Report report;
	private UserOptions userOptions;

	@Before
	public void setup() {
		userOptions = mock(UserOptions.class);
		report = Mockito.mock(Report.class);
		fileRepository = mock(FilePrinter.class);
		statsPrinter = mock(StatsPrinter.class);
		postShip = new PostShip(fileRepository, statsPrinter);
	}

	@Test
	public void should_invoke_repository_to_save_the_report() {
		postShip.wrapUp(report, "Something");

		verify(fileRepository).save(any(String.class), any(String.class));
	}

	@Test
	public void should_print_stats() {
		postShip.prepareAndPrintStats(userOptions, report, 0);
		
		verify(statsPrinter).print(anyString());
	}
	
	@Test public void 
	should_print_message_with_ellapsed_time_half_a_second() {
		postShip.prepareAndPrintStats(userOptions, report, 500);
		
		verify(statsPrinter).print(contains("0.5"));
	}
	
	@Test public void 
	should_print_message_with_ellaped_time_one_second_and_a_half() {
		postShip.prepareAndPrintStats(userOptions, report, 1500);
		
		verify(statsPrinter).print(contains("1.5"));
	}
	
	@Test public void 
	should_print_number_of_lines_read() {
		when(userOptions.linesReadVolume()).thenReturn(666);
		
		postShip.prepareAndPrintStats(userOptions, report, 0);
		
		verify(statsPrinter).print(contains("666"));
	}
	
	@Test public void 
	should_print_total_lines_reported() {
		when(report.totalLines()).thenReturn(999);
		
		postShip.prepareAndPrintStats(userOptions, report, 0);
		
		verify(statsPrinter).print(contains("999"));
	}
}
