package br.com.agsolve.parser.console.controller;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.List;

import com.google.common.base.Splitter;

public class ParametersValidator {
	
	public ValidationCollator validate(String[] parameters) {
		checkArgument(null != parameters && 0 < parameters.length, "Parameters passed are empty");
		
		ValidationCollator validationCollator = new ValidationCollator();
		
		for(String keyValuePair : parameters) {
			validateMissingValue(keyValuePair, validationCollator);
			validateMissingKey(keyValuePair, validationCollator);
		}
		
		return validationCollator;
	}

	private void validateMissingKey(String keyValuePair, ValidationCollator intoCollator) {
		List<String> split = Splitter.on("=").splitToList(keyValuePair);
		
		String key = split.get(0);
		if(isNullOrEmpty(key)) {
			String value = split.size() == 2 ? split.get(1) : "";
			intoCollator.addErrorMessage("Value passed as " + value + " is missing its key");
		}
	}

	private void validateMissingValue(String keyValuePair, ValidationCollator intoCollator) {
		List<String> split = Splitter.on("=").splitToList(keyValuePair);
		
		String value = split.size() == 2 ? split.get(1) : "";
		if(isNullOrEmpty(value)) {
			String key = split.get(0);
			intoCollator.addErrorMessage(key + " has no matching value");
		}
	}
}
