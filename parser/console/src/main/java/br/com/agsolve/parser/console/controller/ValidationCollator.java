package br.com.agsolve.parser.console.controller;

import java.util.ArrayList;
import java.util.List;

public class ValidationCollator {
	
	static final String CARRIAGE_RETURN = System.getProperty("line.separator");

	private List<String> errors = new ArrayList<>();

	public void addErrorMessage(String error) {
		errors.add(error);
	}
	
	public boolean hasErrors() {
		return !errors.isEmpty();
	}

	public int totalErrors() {
		return errors.size();
	}

	public String message() {
		StringBuilder builder = new StringBuilder();
		
		for (String error : errors) {
			builder.append(error);
			builder.append(CARRIAGE_RETURN);
		}
		
		return builder.toString();
	}
}
