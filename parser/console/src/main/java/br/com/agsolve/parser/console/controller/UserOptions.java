package br.com.agsolve.parser.console.controller;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.List;
import java.util.Map;

public class UserOptions {

	private Map<Object, Object> params;

	public UserOptions(Map<Object, Object> params) {
		this.params = params;
	}

	@SuppressWarnings("unchecked")
	public List<String> linesRead() {
		return (List<String>) params.get("linesRead");
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> allUserOptions() {
		return (Map<String, String>) params.get("userOptions");
	}

	public Map<Object, Object> parameters() {
		return params;
	}

	public String fileTargetLocation() {
		@SuppressWarnings("unchecked")
		Map<String, String> userOptions = (Map<String, String>) params.get("userOptions");
		
		String target = userOptions.get("target");		
		if(isNullOrEmpty(target)) {
			StringBuilder builder = new StringBuilder(userOptions.get("source"));
			int endFileNamePosition = builder.lastIndexOf(".");
			builder.insert(endFileNamePosition, "-PARSED");
			target = builder.toString();
		}
		
		return target;
	}

	@SuppressWarnings("unchecked")
	public int linesReadVolume() {
		List<String> lines = (List<String>) params.get("linesRead");
		
		return lines.size();
	}
}
