package br.com.agsolve.parser.console.view;


public class Runner {

	private static ConsoleViewer viewer = new ConsoleViewer();

	public static void main(String[] args) {
		viewer.parse(args);
	}

	static void setViewer(ConsoleViewer viewer) {
		Runner.viewer = viewer;
	}
}
