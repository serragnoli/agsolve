package br.com.agsolve.parser.console.controller;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.agsolve.parser.domain.report.Report;

public class PostShip {

	private FilePrinter fileRepository;
	private StatsPrinter statsPrinter;

	@Autowired
	public PostShip(FilePrinter fileRepository, StatsPrinter statsPrinter) {
		this.fileRepository = fileRepository;
		this.statsPrinter = statsPrinter;
	}

	public void wrapUp(Report report, String into) {
		fileRepository.save(report.outputAsTabSeparated(), into);
	}

	public void prepareAndPrintStats(UserOptions userOptions, Report report, long millisEllapsed) {
		Double seconds = Double.valueOf(millisEllapsed / 1000.0);
		Integer rawLines = Integer.valueOf(userOptions.linesReadVolume());
		Integer reportedLines = Integer.valueOf(report.totalLines());
		
		String message = String.format("Finished in %.1f seconds\nLines read: %d\nTotal reporting lines: %d", seconds, rawLines, reportedLines);
		
		statsPrinter.print(message);
	}
}
