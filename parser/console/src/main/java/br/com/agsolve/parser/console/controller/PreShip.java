package br.com.agsolve.parser.console.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Splitter;

public class PreShip {

	private static final Logger LOG = Logger.getLogger(PreShip.class);
	private static final Map<String, String> TO_ENGLISH = new HashMap<>();

	static {
		TO_ENGLISH.put("origem", "source");
		TO_ENGLISH.put("orijem", "source");
		TO_ENGLISH.put("orijen", "source");
		TO_ENGLISH.put("origen", "source");
		TO_ENGLISH.put("destino", "target");
		TO_ENGLISH.put("destin", "target");
		TO_ENGLISH.put("quebra", "lineBreak");
		TO_ENGLISH.put("queba", "lineBreak");
		TO_ENGLISH.put("kebra", "lineBreak");
		TO_ENGLISH.put("qebra", "lineBreak");
		TO_ENGLISH.put("-qebra", "lineBreak");
		TO_ENGLISH.put("source", "source");
		TO_ENGLISH.put("target", "target");
		TO_ENGLISH.put("linebreak", "lineBreak");
	}

	private FilePrinter fileRepository;
	
	@Autowired
	public PreShip(FilePrinter fileRepository) {
		this.fileRepository = fileRepository;
	}

	public UserOptions prepare(String[] parameters) {
		Map<String, String> userOptions = parseUserOptions(parameters);
		List<String> linesRead = loadFile(userOptions.get("source"));

		Map<Object, Object> params = new HashMap<>();
		params.put("userOptions", userOptions);
		params.put("linesRead", linesRead);

		return new UserOptions(params);
	}
	
	private Map<String, String> parseUserOptions(String[] parameters) {
		Map<String, String> userOptions = new HashMap<>();
		Splitter splitter = Splitter.on("=");

		for (String string : parameters) {
			List<String> split = splitter.splitToList(string);
			String englishKey = TO_ENGLISH.get(split.get(0)
													.toLowerCase());
			String value = split.get(1);

			userOptions.put(englishKey, value);
		}
		return userOptions;
	}

	private List<String> loadFile(String source) {
		LOG.debug("Entering: loadFile -parameters " + source);

		List<String> linesRead = fileRepository.load(source);

		LOG.debug("Exiting: loadFile -returning " + linesRead);
		return linesRead;
	}
}
