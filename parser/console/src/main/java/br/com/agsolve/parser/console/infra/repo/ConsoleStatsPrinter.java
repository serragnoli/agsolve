package br.com.agsolve.parser.console.infra.repo;

import br.com.agsolve.parser.console.controller.StatsPrinter;

public class ConsoleStatsPrinter implements StatsPrinter {

	@Override
	public void print(String message) {
		System.out.println(message);
	}
}
