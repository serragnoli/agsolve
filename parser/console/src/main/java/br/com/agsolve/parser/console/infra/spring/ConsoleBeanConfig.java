package br.com.agsolve.parser.console.infra.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.agsolve.parser.console.controller.FilePrinter;
import br.com.agsolve.parser.console.controller.ParametersValidator;
import br.com.agsolve.parser.console.controller.PostShip;
import br.com.agsolve.parser.console.controller.PreShip;
import br.com.agsolve.parser.console.controller.StatsPrinter;
import br.com.agsolve.parser.console.infra.repo.ConsoleStatsPrinter;
import br.com.agsolve.parser.console.infra.repo.IoFilePrinter;

@Configuration
public class ConsoleBeanConfig {

	@Bean
	public ParametersValidator parametersValidator() {
		return new ParametersValidator();
	}
	
	@Bean
	public PreShip preShip() {
		return new PreShip(fileRepository());
	}
	
	@Bean
	public PostShip postShip() {
		return new PostShip(fileRepository(), statsPrinter());
	}
	
	private FilePrinter fileRepository() {
		return new IoFilePrinter();
	}
	
	private StatsPrinter statsPrinter() {
		return new ConsoleStatsPrinter();
	}
}
