package br.com.agsolve.parser.console.controller;

import org.apache.log4j.Logger;

import br.com.agsolve.parser.actions.CreateReportFromRawLines;
import br.com.agsolve.parser.domain.report.Report;



public class ParseController {
		
	private static final Logger LOG = Logger.getLogger(ParseController.class);
	
	private CreateReportFromRawLines createReportFromRawLines;
	private ParametersValidator validator;
	private PreShip preShip;
	private PostShip postShip;

	public ParseController(CreateReportFromRawLines parseRawListOfLines, ParametersValidator validator, PreShip preShip, PostShip postShip) {
		this.createReportFromRawLines = parseRawListOfLines;
		this.validator = validator;
		this.preShip = preShip;
		this.postShip = postShip;
	}

	public void parse(String[] parameters) {
		LOG.info("Entering: parse");
		LOG.debug("Parameters: " + parameters);
		long startingTime = System.currentTimeMillis();
		
		ValidationCollator validation = validator.validate(parameters);
		if(validation.hasErrors()) {
			throw new IllegalArgumentException(validation.message());
		}
		
		UserOptions readyToShip = preShip.prepare(parameters);		
		
		LOG.info("Invoking Application Service");
		LOG.debug("Parameters: " + readyToShip);
		Report report = createReportFromRawLines.parseRawLines(readyToShip.parameters());
		LOG.info("Finished with Application Service");
		LOG.debug("Returned: " + report);
		
		postShip.wrapUp(report, readyToShip.fileTargetLocation());
		
		long millisEllapsed = System.currentTimeMillis() - startingTime;
		postShip.prepareAndPrintStats(readyToShip, report, millisEllapsed);
	}
}
