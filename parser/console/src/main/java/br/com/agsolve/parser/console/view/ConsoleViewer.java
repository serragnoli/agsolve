package br.com.agsolve.parser.console.view;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import br.com.agsolve.parser.actions.CreateReportFromRawLines;
import br.com.agsolve.parser.console.controller.ParametersValidator;
import br.com.agsolve.parser.console.controller.ParseController;
import br.com.agsolve.parser.console.controller.PostShip;
import br.com.agsolve.parser.console.controller.PreShip;
import br.com.agsolve.parser.console.infra.spring.ConsoleBeanConfig;
import br.com.agsolve.parser.infra.spring.CoreBeanConfig;

public class ConsoleViewer {

	private ParseController parseController;

	ConsoleViewer(ParseController parseController) {
		this.parseController = parseController;
	}

	ConsoleViewer() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsoleBeanConfig.class, CoreBeanConfig.class);
		
		parseController = new ParseController(context.getBean(CreateReportFromRawLines.class), context.getBean(ParametersValidator.class), context.getBean(PreShip.class), context.getBean(PostShip.class));
		
		context.close();		
	}
	
	public void parse(String[] args) {
		parseController.parse(args);
	}
}
