package br.com.agsolve.parser.console.infra.repo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.agsolve.parser.console.controller.FilePrinter;

public class IoFilePrinter implements FilePrinter {

	private static final Logger LOG = Logger.getLogger(IoFilePrinter.class);

	@Override
	public List<String> load(String from) {
		LOG.info("Entering: load");
		LOG.debug("Parameter: " + from);

		List<String> linesRead = new ArrayList<>();

		try {
			Path path = FileSystems.getDefault()
									.getPath(from);
			linesRead = Files.readAllLines(path, Charset.forName("UTF-8"));
		} catch (IOException e) {
			LOG.error(e);
			throw new IllegalArgumentException("File location provided could not be loaded. Please check the location");
		}

		LOG.info("Exiting: load");
		LOG.debug("Returning: " + linesRead);

		return linesRead;
	}

	@Override
	public void save(String outputText, String into) {
		LOG.info("Entering: save");
		LOG.debug("Parameter: text " + outputText + " into " + into);

		Path pathInto = Paths.get(into);

		try {
			Files.write(pathInto, outputText.getBytes());
		} catch (IOException e) {
			LOG.error("Could not write file to the specified location");
			throw new IllegalArgumentException("Could not write file to the specified location");
		}
		
		LOG.info("Exiting: save");
	}
}
