package br.com.agsolve.parser.console.controller;

import java.util.List;

public interface FilePrinter {

	List<String> load(String from);

	void save(String outputText, String into);
}
