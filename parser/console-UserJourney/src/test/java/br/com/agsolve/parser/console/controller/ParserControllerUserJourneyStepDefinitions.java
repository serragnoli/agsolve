package br.com.agsolve.parser.console.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import br.com.agsolve.parser.actions.CreateReportFromRawLines;
import br.com.agsolve.parser.domain.report.Report;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class ParserControllerUserJourneyStepDefinitions {

	private ParseController parseController;
	private CreateReportFromRawLines createReportFromRawLines;
	private ParametersValidator validator;
	private PreShip preShip;
	private PostShip postShip;
	private List<String> parameters;
	private FilePrinter fileRepository;
	
	@Before public void 
	setup() {
		validator = new ParametersValidator();
		fileRepository = mock(FilePrinter.class);
		postShip = mock(PostShip.class);
		preShip = new PreShip(fileRepository);
		createReportFromRawLines = mock(CreateReportFromRawLines.class);
		parseController = new ParseController(createReportFromRawLines, validator, preShip, postShip);
		parameters = new ArrayList<>();
	}
	
	@Given("^the raw file source parameter \"([^\"]*)\"$") public void 
	determine_file_for_parsing(String source) {
		parameters.add(source);
	}
	
	@And("^the destination parameter \"([^\"]*)\" for the file$") public void
	destination_file_supplied(String destination) {
		parameters.add(destination);
	}
	
	@And("^the line break position parameter \"([^\"]*)\"$") public void 
	line_break_supplied(String lineBreak) {
		parameters.add(lineBreak);
	}
	
	@When("^Parser is invoked$") public void
	invoke_parser() {
		parseController.parse(parameters.toArray(new String[]{}));
	}
	
	@Then("^Parser will be provided with the same parameters") public void
	check_parsed_file() {
		verify(createReportFromRawLines).parseRawLines(anyMapOf(Object.class, Object.class));
	}
	
	@Then("^PostShip wraps up the parsing process") public void
	wraps_up_parsing_request(){
		verify(postShip).wrapUp(any(Report.class), any(String.class));
	}
}
