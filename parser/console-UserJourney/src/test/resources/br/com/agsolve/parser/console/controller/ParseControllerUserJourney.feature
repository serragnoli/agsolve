Feature: File Parsing
	As a User
	I want have my report generated according to my parameters
	So that I can analyse the data from the raw file

Scenario: Raw file with two valid lines
	Given the raw file source parameter "origem=./target/test-classes/twoLines.txt"
	And the destination parameter "destino=./target/test-classes/parsedFileFromTwoLines.txt" for the file
	And the line break position parameter "quebra=52"
	When Parser is invoked
	Then Parser will be provided with the same parameters
	Then PostShip wraps up the parsing process